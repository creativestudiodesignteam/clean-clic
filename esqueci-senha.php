<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <?php include("includes/head.php")?>
  </head>
  <body>
    <?php include("includes/load.php")?>
        
    <header class="header-login">
        <nav class="navbar navbar-expand-lg navbar-dark bg-transparent">
            <div class="container-fluid">
                <a class="navbar-brand" href="#home">
                    <img src="assets/images/logo.png" class="logo-login d-none d-md-block" alt="CleanClic" title="CleanClic" />
                    <img src="assets/images/logo-c.png" class="logo-c d-block d-md-none" alt="CleanClic" title="CleanClic" />
                </a>
                <div class="navbar-collapse">
                    <div class="ml-auto my-2 my-lg-0">
                        <ul class="navbar-nav">
                            <li class="nav-item action-menu">
                                <a class="nav-link" href="index.php">Voltar a home</a>
                            </li>
                            <li class="nav-item action-menu">
                                <a class="nav-link" href="cadastro.php">Cadastro
                                    <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                        viewBox="0 0 31.49 31.49" style="enable-background:new 0 0 31.49 31.49;" width="14px" xml:space="preserve">
                                        <path style="fill:#fff;" d="M21.205,5.007c-0.429-0.444-1.143-0.444-1.587,0c-0.429,0.429-0.429,1.143,0,1.571l8.047,8.047H1.111
                                            C0.492,14.626,0,15.118,0,15.737c0,0.619,0.492,1.127,1.111,1.127h26.554l-8.047,8.032c-0.429,0.444-0.429,1.159,0,1.587
                                            c0.444,0.444,1.159,0.444,1.587,0l9.952-9.952c0.444-0.429,0.444-1.143,0-1.571L21.205,5.007z"/>
                                    </svg>
                                </a>
                            </li>
    
                            <li class="nav-item action-menu">
                                <a class="nav-link lang active" href="#.">Pt</a>
                            </li>

                            <li class="nav-item action-menu">
                                <a class="nav-link lang mr-3" href="#.">En</a>
                            </li>

                            <li class="nav-item d-block d-md-none">
                                <a href="index.php" class="contact-fixed">
                                    <svg version="1.1" id="Capa_home" width="25px" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                        viewBox="0 0 495.398 495.398" style="enable-background:new 0 0 495.398 495.398;fill: #fff  ;"
                                        xml:space="preserve">
                                        <g>
                                            <g>
                                                <g>
                                                    <path d="M487.083,225.514l-75.08-75.08V63.704c0-15.682-12.708-28.391-28.413-28.391c-15.669,0-28.377,12.709-28.377,28.391
                                                        v29.941L299.31,37.74c-27.639-27.624-75.694-27.575-103.27,0.05L8.312,225.514c-11.082,11.104-11.082,29.071,0,40.158
                                                        c11.087,11.101,29.089,11.101,40.172,0l187.71-187.729c6.115-6.083,16.893-6.083,22.976-0.018l187.742,187.747
                                                        c5.567,5.551,12.825,8.312,20.081,8.312c7.271,0,14.541-2.764,20.091-8.312C498.17,254.586,498.17,236.619,487.083,225.514z"/>
                                                    <path d="M257.561,131.836c-5.454-5.451-14.285-5.451-19.723,0L72.712,296.913c-2.607,2.606-4.085,6.164-4.085,9.877v120.401
                                                        c0,28.253,22.908,51.16,51.16,51.16h81.754v-126.61h92.299v126.61h81.755c28.251,0,51.159-22.907,51.159-51.159V306.79
                                                        c0-3.713-1.465-7.271-4.085-9.877L257.561,131.836z"/>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                                </a>
                            </li>

                            <li class="nav-item d-block d-md-none">
                                <a href="cadastro.php" class="register-fixed">
                                    <svg viewBox="0 -1 401.52289 401" style="fill: #fff;" width="25px" xmlns="http://www.w3.org/2000/svg">
                                    <path d="m370.589844 250.972656c-5.523438 0-10 4.476563-10 10v88.789063c-.019532 16.5625-13.4375 29.984375-30 30h-280.589844c-16.5625-.015625-29.980469-13.4375-30-30v-260.589844c.019531-16.558594 13.4375-29.980469 30-30h88.789062c5.523438 0 10-4.476563 10-10 0-5.519531-4.476562-10-10-10h-88.789062c-27.601562.03125-49.96875 22.398437-50 50v260.59375c.03125 27.601563 22.398438 49.96875 50 50h280.589844c27.601562-.03125 49.96875-22.398437 50-50v-88.792969c0-5.523437-4.476563-10-10-10zm0 0"/><path d="m376.628906 13.441406c-17.574218-17.574218-46.066406-17.574218-63.640625 0l-178.40625 178.40625c-1.222656 1.222656-2.105469 2.738282-2.566406 4.402344l-23.460937 84.699219c-.964844 3.472656.015624 7.191406 2.5625 9.742187 2.550781 2.546875 6.269531 3.527344 9.742187 2.566406l84.699219-23.464843c1.664062-.460938 3.179687-1.34375 4.402344-2.566407l178.402343-178.410156c17.546875-17.585937 17.546875-46.054687 0-63.640625zm-220.257812 184.90625 146.011718-146.015625 47.089844 47.089844-146.015625 146.015625zm-9.40625 18.875 37.621094 37.625-52.039063 14.417969zm227.257812-142.546875-10.605468 10.605469-47.09375-47.09375 10.609374-10.605469c9.761719-9.761719 25.589844-9.761719 35.351563 0l11.738281 11.734375c9.746094 9.773438 9.746094 25.589844 0 35.359375zm0 0"/></svg>
                                </a>
                            </li>
                        
                            <li class="nav-item d-block d-md-none">
                                <div class="dropdown lang-fixed">
                                    <button id="dLabel" class="dropdown-select" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        PT
                                        <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
                                        <svg class="caret" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                            width="5px" height="5px" viewBox="0 0 292.362 292.362" style="enable-background:new 0 0 292.362 292.362; fill: #5C5C5C;"
                                            xml:space="preserve">
                                            <g>
                                                <path d="M286.935,69.377c-3.614-3.617-7.898-5.424-12.848-5.424H18.274c-4.952,0-9.233,1.807-12.85,5.424
                                                    C1.807,72.998,0,77.279,0,82.228c0,4.948,1.807,9.229,5.424,12.847l127.907,127.907c3.621,3.617,7.902,5.428,12.85,5.428
                                                    s9.233-1.811,12.847-5.428L286.935,95.074c3.613-3.617,5.427-7.898,5.427-12.847C292.362,77.279,290.548,72.998,286.935,69.377z"/>
                                            </g>
                                        </svg>  
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dLabel">
                                        <li>PT</li>
                                        <li>EN</li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>

        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-7 login align-self-center">
                    
                </div>

                <div class="col-10 col-lg-3 offset-1 align-self-center">
                    <h1 class="title-form">Esqueci a<br><span class="bold-title">minha senha</span></h1>
                    <form id="contact-form">
                        <div class="form-row">
                            <div class="form-group input-material col-lg-12">
                                <input type="email" class="form-control" id="name-field" required>
                                <label for="name-field">Digite seu e-mail</label>
                            </div>
                            <div class="form-group col-lg-12 send mt-4">
                                <a href="#." class="cta-dark mt-3 button" data-dismiss="modal">
                                    <span>Enviar</span>
                                    <svg width="13px" height="10px" viewBox="0 0 13 10">
                                        <path d="M1,5 L11,5"></path>
                                        <polyline points="8 1 12 5 8 9"></polyline>
                                    </svg>
                                </a>
                                <a href="login.php" class="forgot">Voltar</a>
                            </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </header>
    <?php include("includes/scripts.php")?>
    
  </body>
</html>