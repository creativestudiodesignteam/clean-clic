<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <?php include("includes/head.php")?>
  </head>
  <body>
    <?php include("includes/load.php")?>
    <?php include("includes/menu.php")?>
    <?php include("includes/menu-overlay.php")?>
    <?php include("includes/slide.php")?>
        
    <section class="about" id="sobre">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-5 offset-lg-1 align-self-center wow fadeInLeft" data-wow-duration="1s" data-wow-delay=".3s">
                    <h2 class="title">Nosso propósito é democratizar o<br><span class="bold-title">acesso à energia limpa e renovável.</span></h2>
                    <div class="text">
                        <p>
                        Acreditamos que compartilhar seja a chave para uma sociedade justa e responsável, entendemos que os atos mais simples, como o “clic” para acender uma lâmpada façam grande diferença se realizados de modo consciente.
                        </p>

                        <p>
                        Gerando energia elétrica de forma compartilhada, facilitamos o acesso a quem não tem condições técnicas ou financeiras para um sistema de geração de energia de fonte renovável. Nosso propósito é disseminar o que temos de melhor, boas energias, boas práticas, boas atitudes...
                        </p>

                        <p>Compartilhe sua energia com a gente. Seja um clic!</p>
                    </div>
                </div>

                <div class="col-lg-6 slide wow fadeInRight align-self-center" data-wow-duration="1s" data-wow-delay=".5s">
                    <div class="about-slide owl-carousel owl-theme">
                        <div class="item">
                            <img src="assets/images/slide-01.jpg" class="image-slider"/>
                        </div>
                        
                        <div class="item">
                            <img src="assets/images/slide-02.jpg" class="image-slider"/>
                        </div>

                        <div class="item">
                            <img src="assets/images/slide-03.jpg" class="image-slider"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="video" id="video">
        <div class="container-fluid">
            <div class="row position-relative">
                <div class="col-lg-7 video-popup wow fadeInLeft" data-wow-duration="1s" data-wow-delay=".7s">
                    <a href="#." class="thumbnail" data-toggle="modal" data-target="#youtubeVideo">
                        <div class="img">
                            <div class="play pulse-button">
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                    viewBox="0 0 41.999 41.999" style="enable-background:new 0 0 41.999 41.999;" width="30px" xml:space="preserve">
                                    <path d="M36.068,20.176l-29-20C6.761-0.035,6.363-0.057,6.035,0.114C5.706,0.287,5.5,0.627,5.5,0.999v40
                                        c0,0.372,0.206,0.713,0.535,0.886c0.146,0.076,0.306,0.114,0.465,0.114c0.199,0,0.397-0.06,0.568-0.177l29-20
                                        c0.271-0.187,0.432-0.494,0.432-0.823S36.338,20.363,36.068,20.176z"/>
                                </svg>
                            </div>
                            <img src="assets/images/bg-video.jpg" class="img-fluid image-video"/>
                        </div>
                    </a>
                </div>

                <div class="col-lg-6 right box-video wow fadeInRight" data-wow-duration="1s" data-wow-delay=".8s">
                    <div class="box">
                        <div class="info">
                            <h1 class="title mb-4">Acreditamos que compartilhar<br><span class="bold-title">seja a chave para uma sociedade justa e responsável</span></h1>
                            <a href="#contato" class="cta-2">
                                <span>Entre em contato</span>
                                <svg width="13px" height="10px" viewBox="0 0 13 10">
                                    <path d="M1,5 L11,5"></path>
                                    <polyline points="8 1 12 5 8 9"></polyline>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="simulator" id="simulador">
        <div class="container-fluid">
            <div class="row position-relative">
                <div class="col-lg-4 offset-lg-1 wow fadeInRight" data-wow-duration="1s" data-wow-delay="1.5s">
                    <div class="simulator-ranged">
                        <h1 class="title">Para simular sua<br><span class="bold-title">economia selecione as opções:</span></h1>
                        <div class="wrapper typo">Qual seu estado
                            <div class="list"><span class="placeholder">Selecione</span>
                                <ul class="list__ul">
                                <li><a href="">São Paulo</a></li>
                                <li><a href="">Bahia</a></li>
                                <li><a href="">Sergipe</a></li>
                                <li><a href="">Tocantins</a></li>
                                <li><a href="">Recife</a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="wrapper-2 typo-2">Empresa de energia
                            <div class="list-2"><span class="placeholder-2">Selecione</span>
                                <ul class="list__ul-2">
                                    <li><a href="">AES</a></li>
                                    <li><a href="">Enel</a></li>
                                </ul>
                            </div>
                        </div>
                        
                        <div class="ranged">
                            <h4 class="title-ranged">Gasto médio mensal</h4>
                            <div class="range-slider" id="valor-real">
                                <input type="range" orient="vertical" min="0" max="100" id="ranger"/>
                                <!-- <input type="range" orient="horizontal" class="d-block d-md-none" min="0" max="100" id="ranger"/> -->
                                <div class="range-slider__bar"></div>
                                <div class="range-slider__thumb"></div>
                            </div>

                            <div class="box-calc-valor d-block d-md-none">
                                <h6 class="title-calc-valor">Gasto médio<br>mensal</h6>

                                <div class="aux-value">
                                    <span class="mounth-valor" id="mounth-value">R$ 605,00</span><br><span>ao mês</span>
                                </div>

                                <div class="bar"></div>

                                <div class="aux-economy">
                                    <span class="economy-valor" id="economy-value">9,50%</span><br><span>de economia</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="images-random d-none d-lg-block">
                    <img class="img-responsive photos-radom wow fadeInRight" id="photos-radom" data-wow-duration="1s" data-wow-delay="1s"/>
                </div>

                <div class="col-lg-4 offset-lg-3 calc-range wow fadeInRight" data-wow-duration="1s" data-wow-delay=".8s">
                    <div class="box-clean">
                        <div class="d-flex align-items-start flex-column" style="height: 100%; max-height: 95%;">
                            <div class="">
                                <h2 class="title">Plano <span class="bold-title">CleanClic</span></h2>
                                <h4 class="subtitle">Temos um plano de <span class="bold-title">ECONOMIA</span> para você, seu gasto médio passa a ser:</h4>
                                <div class="bar"></div>
                                <div class="aux-value">
                                    <span class="mounth-valor text-light" id="mounth-value">R$ 605,00</span><br><span class="text-light">ao mês</span>
                                </div>

                                <div class="bar"></div>
                                
                                <div class="aux-economy">
                                    <span class="economy-valor text-light" id="economy-value">9,50%</span><br><span class="text-light">de economia</span>
                                </div>
                               
                            </div>


                            <div class="mt-auto button-simulator">
                                <a href="#contato" class="cta-2">
                                    <span>Quero ser um Clic</span>
                                    <svg width="13px" height="10px" viewBox="0 0 13 10">
                                        <path d="M1,5 L11,5"></path>
                                        <polyline points="8 1 12 5 8 9"></polyline>
                                    </svg>
                                </a>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="clic" id="clic">
        <div class="container">
            <div class="row informations">
                <h1 class="hastag">#sejaumclic</h1>
                <div class="col-lg-4 wow fadeInUp" data-wow-duration="1s" data-wow-delay=".4s">
                    <div class="card economy">
                        <div class="card-block">
                        <svg version="1.1" id="Layer_1" class="mb-3" width="70px" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                            viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;fill: #fff;" xml:space="preserve">
                                <g>
                                    <g>
                                        <path d="M260.348,352.204c-4.503,0-8.153,3.65-8.153,8.153v17.39c0,4.503,3.65,8.153,8.153,8.153s8.153-3.65,8.153-8.153v-17.39
                                            C268.501,355.854,264.851,352.204,260.348,352.204z"/>
                                    </g>
                                </g>
                                <g>
                                    <g>
                                        <path d="M507.064,235.387c-3.182-3.182-7.411-4.934-11.912-4.934c-0.001,0-0.002,0-0.003,0l-20.929,0.003
                                            c-0.265,0-0.447-0.298-0.503-0.574c-5.753-28.218-18.654-54.82-37.31-76.932c-18.032-21.372-41.127-38.18-66.88-48.737
                                            c-4.154-17.536-12.925-35.616-30.222-49.207c-16.746-13.158-33.612-15.69-34.321-15.792c-2.336-0.333-4.709,0.363-6.495,1.912
                                            c-1.786,1.549-2.811,3.795-2.811,6.159v44.026h-29.588c-7.312-25.09-30.5-43.482-57.919-43.482
                                            c-28.001,0-51.603,19.177-58.376,45.085c-40.312,5.491-77.4,25.021-104.92,55.386C15.937,180.233,0,221.569,0,264.696
                                            c0,96.801,50.255,175.564,65.659,197.397c4.758,6.745,12.551,10.773,20.846,10.773h34.701c14.086,0,25.546-11.46,25.546-25.546
                                            v-11.285c8.781,1.352,17.702,2.045,26.633,2.045h78.811v9.24c0,14.086,11.46,25.546,25.546,25.546h38.117
                                            c11.966,0,22.471-8.493,24.978-20.193l4.184-19.524c53.584-13.049,96.667-50.641,117.872-99.424h32.258
                                            c9.291,0,16.849-7.558,16.849-16.849v-69.572C512,242.801,510.248,238.57,507.064,235.387z M208.17,64.136
                                            c24.276,0,44.025,19.75,44.025,44.026c0,6.208-1.329,12.323-3.833,17.936h-80.385c-2.503-5.615-3.833-11.728-3.833-17.936
                                            C164.144,83.885,183.894,64.136,208.17,64.136z M495.694,316.874c0,0.3-0.243,0.544-0.544,0.544h-26.128
                                            c4.441-13.904,7.174-28.526,7.956-43.604c0.234-4.496-3.223-8.331-7.72-8.564c-4.497-0.228-8.331,3.223-8.564,7.72
                                            c-3.454,66.592-48.778,123.449-111.832,142.252l11.391-53.155c0.944-4.403-1.861-8.737-6.264-9.68
                                            c-4.4-0.946-8.737,1.861-9.68,6.264l-19.416,90.604c-0.907,4.232-4.707,7.304-9.034,7.304h-38.119c-5.095,0-9.24-4.145-9.24-9.24
                                            v-34.786c0-4.503-3.65-8.153-8.153-8.153s-8.153,3.65-8.153,8.153v9.24h-78.811c-8.945,0-17.874-0.769-26.633-2.262v-59.155
                                            c0-4.503-3.65-8.153-8.153-8.153s-8.153,3.65-8.153,8.153v86.964c0,5.095-4.145,9.24-9.24,9.24H86.505
                                            c-3.048,0-5.79-1.409-7.522-3.866c-14.703-20.843-62.677-95.997-62.677-187.999c0-39.072,14.437-76.52,40.651-105.445
                                            c23.99-26.473,55.999-43.853,90.907-49.561c0.141,5.587,1.057,11.111,2.701,16.407h-3.27c-4.503,0-8.153,3.65-8.153,8.153
                                            s3.65,8.153,8.153,8.153h121.75c4.503,0,8.153-3.65,8.153-8.153s-3.65-8.153-8.153-8.153h-3.271
                                            c1.795-5.779,2.727-11.828,2.727-17.936c0-0.183-0.012-0.362-0.014-0.544h27.19v17.936c0,4.503,3.65,8.153,8.153,8.153
                                            c4.503,0,8.153-3.65,8.153-8.153V57.99c15.469,5.854,44.569,23.835,44.569,76.26c0,4.503,3.65,8.153,8.153,8.153
                                            s8.153-3.65,8.153-8.153c0-3.454-0.125-7.101-0.411-10.87c19.667,9.58,37.311,23.272,51.498,40.087
                                            c16.9,20.031,28.586,44.123,33.795,69.673c1.609,7.895,8.541,13.624,16.483,13.623l20.929-0.002c0.055,0,0.225,0,0.384,0.159
                                            c0.159,0.159,0.159,0.327,0.159,0.384V316.874z"/>
                                    </g>
                                </g>
                                <g>
                                    <g>
                                        <path d="M424.521,235.759c-5.06-13.576-18.455-22.698-33.327-22.698c-14.873,0-28.266,9.121-33.329,22.698
                                            c-1.573,4.219,0.572,8.914,4.791,10.487c4.22,1.575,8.915-0.572,10.487-4.791c2.696-7.23,9.95-12.088,18.05-12.088
                                            s15.355,4.858,18.05,12.088c1.223,3.281,4.333,5.307,7.64,5.307c0.947,0,1.909-0.166,2.847-0.516
                                            C423.949,244.673,426.094,239.977,424.521,235.759z"/>
                                    </g>
                                </g>
                                <g>
                                    <g>
                                        <path d="M216.323,258.479v-41.664c15.994,1.944,27.176,9.354,27.176,16.085c0,4.503,3.65,8.153,8.153,8.153
                                            c4.503,0,8.153-3.65,8.153-8.153c0-16.668-18.534-30.069-43.482-32.49v-0.937c0-4.503-3.65-8.153-8.153-8.153
                                            s-8.153,3.65-8.153,8.153v0.937c-24.948,2.421-43.482,15.822-43.482,32.49c0,23.807,23.52,32.399,43.482,38.013v41.664
                                            c-15.994-1.944-27.176-9.354-27.176-16.085c0-4.503-3.65-8.153-8.153-8.153s-8.153,3.65-8.153,8.153
                                            c0,16.668,18.534,30.069,43.482,32.49v0.937c0,4.503,3.65,8.153,8.153,8.153s8.153-3.65,8.153-8.153v-0.937
                                            c24.948-2.421,43.482-15.822,43.482-32.49C259.805,272.684,236.284,264.093,216.323,258.479z M200.017,253.843
                                            c-20.08-6.299-27.176-12.023-27.176-20.943c0-6.731,11.182-14.141,27.176-16.085V253.843z M216.323,312.578v-37.028
                                            c20.08,6.298,27.176,12.023,27.176,20.943C243.499,303.224,232.316,310.634,216.323,312.578z"/>
                                    </g>
                                </g>
                        </svg>
                            
                            <h1 class="title">Saiba como começar<br><span class="bold-title">a economizar com a</span></h1>
                            <img src="assets/images/logo.png" class="img-fluid"/>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 wow fadeInUp" data-wow-duration="1s" data-wow-delay=".7s">
                    <div class="card checklist">
                        <div class="card-block">
                            <div class="row">
                                <div class="col-7 offset-3 col-lg-4 offset-lg-2 pt-5">
                                    <ul class="list-unstyled check">
                                        <li><p>Ser titular da sua fatura de energia.</p></li>
                                        <li><p>Criar uma conta CleanClic e enviar para gente uma foto da sua útlima fatura.</p></li>
                                        <li><p>Ser um cooperado Sicoob.</p></li>
                                        <li><p>Depois análise da fatura escolher o plano que melhor atende sua necessidade.</p></li>
                                        <li><p>Você só começa a pagar a mensalidade quando sua instalação começas a receber o desconto.</p></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="usina" id="usina">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 wow fadeInUp" data-wow-duration="1s" data-wow-delay=".8s">
                    <div class="card">
                        <img class="card-img d-none d-md-block" src="assets/images/bg-usina.jpg" />
                        <img class="card-img d-block d-md-none" src="assets/images/bg-usina-mobile.jpg" />
                        <div class="card-img-overlay d-flex flex-column">
                            <div class="mt-auto">
                                <svg version="1.1" id="Capa_1" class="mb-4" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                    viewBox="0 0 511.998 511.998" width="70px" style="enable-background:new 0 0 511.998 511.998; fill: #fff;" xml:space="preserve">
                                        <g>
                                            <g>
                                                <path d="M255.998,40.32c-20.833,0-37.781,16.948-37.781,37.781s16.948,37.78,37.781,37.78s37.781-16.949,37.781-37.78
                                                    C293.78,57.268,276.831,40.32,255.998,40.32z M255.998,100.802c-12.518,0-22.703-10.184-22.703-22.702
                                                    s10.184-22.703,22.703-22.703s22.703,10.184,22.703,22.703S268.517,100.802,255.998,100.802z"/>
                                            </g>
                                        </g>
                                        <g>
                                            <g>
                                                <path d="M255.998,0c-4.165,0-7.539,3.376-7.539,7.539v10.08c0,4.164,3.375,7.539,7.539,7.539s7.539-3.376,7.539-7.539V7.539
                                                    C263.538,3.376,260.163,0,255.998,0z"/>
                                            </g>
                                        </g>
                                        <g>
                                            <g>
                                                <path d="M218.563,30.002l-7.128-7.127c-2.943-2.944-7.717-2.943-10.662,0c-2.944,2.944-2.944,7.718,0.001,10.663l7.127,7.128
                                                    c1.472,1.472,3.402,2.208,5.331,2.208s3.859-0.736,5.332-2.209C221.508,37.72,221.508,32.946,218.563,30.002z"/>
                                            </g>
                                        </g>
                                        <g>
                                            <g>
                                                <path d="M195.518,70.56h-10.08c-4.165,0-7.539,3.376-7.539,7.539c0,4.164,3.375,7.539,7.539,7.539h10.08
                                                    c4.165,0,7.539-3.376,7.539-7.539C203.057,73.936,199.682,70.56,195.518,70.56z"/>
                                            </g>
                                        </g>
                                        <g>
                                            <g>
                                                <path d="M218.563,115.536c-2.945-2.944-7.718-2.944-10.664,0l-7.127,7.127c-2.944,2.944-2.944,7.718,0,10.662
                                                    c1.473,1.472,3.403,2.209,5.332,2.209s3.859-0.736,5.332-2.209l7.127-7.127C221.507,123.254,221.507,118.48,218.563,115.536z"/>
                                            </g>
                                        </g>
                                        <g>
                                            <g>
                                                <path d="M255.998,131.042c-4.165,0-7.539,3.376-7.539,7.539v10.08c0,4.164,3.375,7.539,7.539,7.539s7.539-3.376,7.539-7.539
                                                    v-10.08C263.538,134.418,260.163,131.042,255.998,131.042z"/>
                                            </g>
                                        </g>
                                        <g>
                                            <g>
                                                <path d="M311.225,122.665l-7.127-7.127c-2.944-2.944-7.718-2.945-10.663-0.001c-2.945,2.944-2.945,7.718-0.001,10.663l7.127,7.127
                                                    c1.473,1.472,3.402,2.208,5.332,2.208c1.929,0,3.859-0.736,5.331-2.208C314.169,130.383,314.169,125.609,311.225,122.665z"/>
                                            </g>
                                        </g>
                                        <g>
                                            <g>
                                                <path d="M326.56,70.562h-10.08c-4.165,0-7.539,3.376-7.539,7.539s3.375,7.539,7.539,7.539h10.08c4.165,0,7.539-3.376,7.539-7.539
                                                    S330.725,70.562,326.56,70.562z"/>
                                            </g>
                                        </g>
                                        <g>
                                            <g>
                                                <path d="M311.224,22.874c-2.943-2.944-7.717-2.943-10.662,0l-7.127,7.127c-2.944,2.944-2.944,7.718,0.001,10.663
                                                    c1.471,1.473,3.401,2.209,5.33,2.209c1.929,0,3.859-0.736,5.332-2.209l7.127-7.127C314.169,30.593,314.169,25.819,311.224,22.874z
                                                    "/>
                                            </g>
                                        </g>
                                        <g>
                                            <g>
                                                <path d="M465.123,386.181c-0.006-0.132-0.007-0.263-0.02-0.395c-0.026-0.265-0.066-0.526-0.12-0.782
                                                    c-0.001-0.006-0.001-0.011-0.003-0.017l-13.427-63.778c-0.005-0.023-0.01-0.045-0.014-0.068l-13.426-63.774
                                                    c-0.005-0.023-0.01-0.045-0.014-0.068l-13.439-63.837c-0.734-3.489-3.812-5.986-7.378-5.986H94.713
                                                    c-3.566,0-6.643,2.497-7.378,5.986l-13.436,63.83c-0.006,0.027-0.012,0.053-0.017,0.08l-13.425,63.775
                                                    c-0.004,0.018-0.008,0.037-0.011,0.055L47.02,384.981c-0.056,0.269-0.099,0.544-0.126,0.822c-0.006,0.064-0.005,0.13-0.01,0.194
                                                    c-0.013,0.179-0.027,0.358-0.027,0.541v30.243c0,4.164,3.375,7.539,7.539,7.539h178.94v42.356h-57.982
                                                    c-4.165,0-7.539,3.376-7.539,7.539v22.704H94.714c-4.165,0-7.539,3.376-7.539,7.539s3.375,7.539,7.539,7.539h80.641h161.285h80.63
                                                    c4.165,0,7.539-3.376,7.539-7.539s-3.375-7.539-7.539-7.539H344.18v-22.704c0-4.164-3.375-7.539-7.539-7.539h-57.982V424.32H457.6
                                                    c4.165,0,7.539-3.376,7.539-7.539v-30.108c0.001-0.01,0.001-0.022,0.001-0.034v-0.098
                                                    C465.141,386.418,465.129,386.301,465.123,386.181z M448.311,379H330.23l-3.423-48.763h111.238L448.311,379z M424.605,266.396
                                                    l10.267,48.763H325.749l-3.423-48.763H424.605z M411.165,202.555l10.267,48.763H321.268l-3.423-48.763H411.165z M209.265,202.555
                                                    h93.465l3.423,48.763h-100.31L209.265,202.555z M204.786,266.396h102.425l3.423,48.763h-109.27L204.786,266.396z M200.307,330.238
                                                    h111.385L315.114,379H196.885L200.307,330.238z M100.831,202.555h93.319l-3.422,48.763H90.565L100.831,202.555z M87.392,266.396
                                                    H189.67l-3.422,48.763H77.126L87.392,266.396z M329.102,481.757v15.164H182.895v-15.164h57.982h30.244H329.102z M248.416,466.678
                                                    v-42.356h15.165v42.356H248.416z M271.208,409.243c-0.029,0-0.058-0.004-0.087-0.004h-30.244c-0.029,0-0.058,0.004-0.087,0.004
                                                    H61.935v-15.164h35.915c4.165,0,7.539-3.376,7.539-7.539c0-4.164-3.375-7.539-7.539-7.539H63.687l10.265-48.763H185.19
                                                    L181.768,379h-51.314h-0.001c-4.165,0-7.539,3.376-7.539,7.539c0,4.164,3.375,7.539,7.539,7.539h0.001h58.337
                                                    c0.005,0,0.009,0,0.014,0c0.003,0,0.007,0,0.01,0h134.367c0.003,0,0.007,0,0.01,0c0.005,0,0.009,0,0.014,0H450.06v15.164H271.208z
                                                    "/>
                                            </g>
                                        </g>
                                </svg>

                                <h1 class="title">Financie<br><span class="bold-title">sua Usina</span></h1>
                                <p class="description">Em breve</p>
                                <!-- <a href="#contato" class="cta button">
                                    <span>Quero saber mais</span>
                                    <svg width="13px" height="10px" viewBox="0 0 13 10">
                                        <path d="M1,5 L11,5"></path>
                                        <polyline points="8 1 12 5 8 9"></polyline>
                                    </svg>
                                </a> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="faq" id="faq">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 wow fadeInLeft" data-wow-duration="1s" data-wow-delay=".5s">
                    <h1 class="title">Está com dúvidas?<br><span class="bold-title">Vaja em nosso FAQ</span></h1>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-11 offset-lg-1 informations-faq wow fadeInRight" data-wow-duration="1s" data-wow-delay=".8s">
                    <div class="faq-slide owl-carousel owl-theme">
                        <div class="item">
                            <div class="flip">
                                <div class="front d-flex flex-column" style="background-color: #fff;">
                                    <div class="mt-auto">
                                        <svg version="1.1" id="Capa_1" class="mb-3" width="60px" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                            viewBox="0 0 512 512" style="enable-background:new 0 0 512 512; fill: #18A074;" xml:space="preserve">
                                                <g>
                                                    <g>
                                                        <path d="M346,319c-5.522,0-10,4.477-10,10v69c0,27.57-22.43,50-50,50H178.032c-5.521,0-9.996,4.473-10,9.993l-0.014,19.882
                                                            l-23.868-23.867c-1.545-3.547-5.081-6.008-9.171-6.008H70c-27.57,0-50-22.43-50-50V244c0-27.57,22.43-50,50-50h101
                                                            c5.522,0,10-4.477,10-10s-4.478-10-10-10H70c-38.598,0-70,31.402-70,70v154c0,38.598,31.402,70,70,70h59.858l41.071,41.071
                                                            c1.913,1.913,4.47,2.929,7.073,2.929c1.287,0,2.586-0.249,3.821-0.76c3.737-1.546,6.174-5.19,6.177-9.233L188.024,468H286
                                                            c38.598,0,70-31.402,70-70v-69C356,323.477,351.522,319,346,319z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M366.655,0h-25.309C261.202,0,196,65.202,196,145.346s65.202,145.345,145.345,145.345h25.309
                                                            c12.509,0,24.89-1.589,36.89-4.729l37.387,37.366c1.913,1.911,4.469,2.927,7.071,2.927c1.289,0,2.589-0.249,3.826-0.762
                                                            c3.736-1.548,6.172-5.194,6.172-9.238v-57.856c15.829-12.819,28.978-29.012,38.206-47.102
                                                            C506.687,190.751,512,168.562,512,145.346C512,65.202,446.798,0,366.655,0z M441.983,245.535
                                                            c-2.507,1.889-3.983,4.847-3.983,7.988v38.6l-24.471-24.458c-1.904-1.902-4.458-2.927-7.07-2.927c-0.98,0-1.97,0.145-2.936,0.442
                                                            c-11.903,3.658-24.307,5.512-36.868,5.512h-25.309c-69.117,0-125.346-56.23-125.346-125.346S272.23,20,341.346,20h25.309
                                                            C435.771,20,492,76.23,492,145.346C492,185.077,473.77,221.595,441.983,245.535z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M399.033,109.421c-1.443-20.935-18.319-37.811-39.255-39.254c-11.868-0.815-23.194,3.188-31.863,11.281
                                                            c-8.55,7.981-13.453,19.263-13.453,30.954c0,5.523,4.478,10,10,10c5.522,0,10-4.477,10-10c0-6.259,2.522-12.06,7.1-16.333
                                                            c4.574-4.269,10.552-6.382,16.842-5.948c11.028,0.76,19.917,9.649,20.677,20.676c0.768,11.137-6.539,20.979-17.373,23.403
                                                            c-8.778,1.964-14.908,9.592-14.908,18.549v24.025c0,5.523,4.478,10,10,10c5.523,0,10-4.477,9.999-10v-23.226
                                                            C386.949,148.68,400.468,130.242,399.033,109.421z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M363.87,209.26c-1.86-1.86-4.44-2.93-7.07-2.93s-5.21,1.07-7.07,2.93c-1.86,1.86-2.93,4.44-2.93,7.07
                                                            c0,2.64,1.071,5.22,2.93,7.08c1.86,1.86,4.44,2.92,7.07,2.92s5.21-1.06,7.07-2.92c1.86-1.87,2.93-4.44,2.93-7.08
                                                            C366.8,213.7,365.729,211.12,363.87,209.26z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M275,310H64c-5.522,0-10,4.477-10,10s4.478,10,10,10h211c5.523,0,10-4.477,10-10S280.522,310,275,310z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M282.069,368.93C280.21,367.07,277.63,366,275,366s-5.21,1.07-7.07,2.93c-1.861,1.86-2.93,4.44-2.93,7.07
                                                            s1.07,5.21,2.93,7.07c1.86,1.86,4.44,2.93,7.07,2.93s5.21-1.07,7.069-2.93c1.861-1.86,2.931-4.43,2.931-7.07
                                                            C285,373.37,283.929,370.79,282.069,368.93z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M235.667,366H64c-5.522,0-10,4.477-10,10s4.478,10,10,10h171.667c5.523,0,10-4.477,10-10S241.189,366,235.667,366z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M210,254H64c-5.522,0-10,4.477-10,10s4.478,10,10,10h146c5.523,0,10-4.477,10-10S215.522,254,210,254z"/>
                                                    </g>
                                                </g>
                                        </svg>
                                        <h4 class="title-front">O que é Geração de energia?</h4>
                                    </div>
                                </div>
                                <div class="back d-flex flex-column">
                                    <div class="mt-auto">
                                        <h5 class="title-back">O que é Geração de energia?</h5>
                                        <p>A geração compartilhada é uma das modalidades do Sistema de Compensação de Energia Elétrica, criado pela Resolução 482/ 2012 da ANEEL (Agência Nacional de Energia Elétrica). Na geração compartilhada, um grupo de consumidores reunidos em uma cooperativa ou consórcio, dentro da mesma área de concessão, divide entre si os créditos de energia gerados por uma usina de micro ou minigeração.</p>                                    
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="item">
                            <div class="flip">
                                <div class="front d-flex flex-column" style="background-color: #fff;">
                                    <div class="mt-auto">
                                        <svg version="1.1" id="Capa_1" class="mb-3" width="60px" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                            viewBox="0 0 512 512" style="enable-background:new 0 0 512 512; fill: #18A074;" xml:space="preserve">
                                                <g>
                                                    <g>
                                                        <path d="M346,319c-5.522,0-10,4.477-10,10v69c0,27.57-22.43,50-50,50H178.032c-5.521,0-9.996,4.473-10,9.993l-0.014,19.882
                                                            l-23.868-23.867c-1.545-3.547-5.081-6.008-9.171-6.008H70c-27.57,0-50-22.43-50-50V244c0-27.57,22.43-50,50-50h101
                                                            c5.522,0,10-4.477,10-10s-4.478-10-10-10H70c-38.598,0-70,31.402-70,70v154c0,38.598,31.402,70,70,70h59.858l41.071,41.071
                                                            c1.913,1.913,4.47,2.929,7.073,2.929c1.287,0,2.586-0.249,3.821-0.76c3.737-1.546,6.174-5.19,6.177-9.233L188.024,468H286
                                                            c38.598,0,70-31.402,70-70v-69C356,323.477,351.522,319,346,319z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M366.655,0h-25.309C261.202,0,196,65.202,196,145.346s65.202,145.345,145.345,145.345h25.309
                                                            c12.509,0,24.89-1.589,36.89-4.729l37.387,37.366c1.913,1.911,4.469,2.927,7.071,2.927c1.289,0,2.589-0.249,3.826-0.762
                                                            c3.736-1.548,6.172-5.194,6.172-9.238v-57.856c15.829-12.819,28.978-29.012,38.206-47.102
                                                            C506.687,190.751,512,168.562,512,145.346C512,65.202,446.798,0,366.655,0z M441.983,245.535
                                                            c-2.507,1.889-3.983,4.847-3.983,7.988v38.6l-24.471-24.458c-1.904-1.902-4.458-2.927-7.07-2.927c-0.98,0-1.97,0.145-2.936,0.442
                                                            c-11.903,3.658-24.307,5.512-36.868,5.512h-25.309c-69.117,0-125.346-56.23-125.346-125.346S272.23,20,341.346,20h25.309
                                                            C435.771,20,492,76.23,492,145.346C492,185.077,473.77,221.595,441.983,245.535z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M399.033,109.421c-1.443-20.935-18.319-37.811-39.255-39.254c-11.868-0.815-23.194,3.188-31.863,11.281
                                                            c-8.55,7.981-13.453,19.263-13.453,30.954c0,5.523,4.478,10,10,10c5.522,0,10-4.477,10-10c0-6.259,2.522-12.06,7.1-16.333
                                                            c4.574-4.269,10.552-6.382,16.842-5.948c11.028,0.76,19.917,9.649,20.677,20.676c0.768,11.137-6.539,20.979-17.373,23.403
                                                            c-8.778,1.964-14.908,9.592-14.908,18.549v24.025c0,5.523,4.478,10,10,10c5.523,0,10-4.477,9.999-10v-23.226
                                                            C386.949,148.68,400.468,130.242,399.033,109.421z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M363.87,209.26c-1.86-1.86-4.44-2.93-7.07-2.93s-5.21,1.07-7.07,2.93c-1.86,1.86-2.93,4.44-2.93,7.07
                                                            c0,2.64,1.071,5.22,2.93,7.08c1.86,1.86,4.44,2.92,7.07,2.92s5.21-1.06,7.07-2.92c1.86-1.87,2.93-4.44,2.93-7.08
                                                            C366.8,213.7,365.729,211.12,363.87,209.26z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M275,310H64c-5.522,0-10,4.477-10,10s4.478,10,10,10h211c5.523,0,10-4.477,10-10S280.522,310,275,310z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M282.069,368.93C280.21,367.07,277.63,366,275,366s-5.21,1.07-7.07,2.93c-1.861,1.86-2.93,4.44-2.93,7.07
                                                            s1.07,5.21,2.93,7.07c1.86,1.86,4.44,2.93,7.07,2.93s5.21-1.07,7.069-2.93c1.861-1.86,2.931-4.43,2.931-7.07
                                                            C285,373.37,283.929,370.79,282.069,368.93z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M235.667,366H64c-5.522,0-10,4.477-10,10s4.478,10,10,10h171.667c5.523,0,10-4.477,10-10S241.189,366,235.667,366z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M210,254H64c-5.522,0-10,4.477-10,10s4.478,10,10,10h146c5.523,0,10-4.477,10-10S215.522,254,210,254z"/>
                                                    </g>
                                                </g>
                                        </svg>
                                        <h4 class="title-front">Como isso funciona na prática?</h4>
                                    </div>
                                </div>
                                <div class="back d-flex flex-column">
                                    <div class="mt-auto">
                                        <h5 class="title-back">Como isso funciona na prática?</h5>
                                        <p>É simples: a energia solar é captada por placas fotovoltaicas, instaladas na usina, que a transformam em energia elétrica. Essa energia elétrica gerada na usina é distribuída para a rede elétrica, e a quantidade de energia que foi injetada na rede de distribuição é compensada na conta de luz do cliente que adquiriu cotas daquela usina.</p>                                    
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="item">
                            <div class="flip">
                                <div class="front d-flex flex-column" style="background-color: #fff;">
                                    <div class="mt-auto">
                                        <svg version="1.1" id="Capa_1" class="mb-3" width="60px" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                            viewBox="0 0 512 512" style="enable-background:new 0 0 512 512; fill: #18A074;" xml:space="preserve">
                                                <g>
                                                    <g>
                                                        <path d="M346,319c-5.522,0-10,4.477-10,10v69c0,27.57-22.43,50-50,50H178.032c-5.521,0-9.996,4.473-10,9.993l-0.014,19.882
                                                            l-23.868-23.867c-1.545-3.547-5.081-6.008-9.171-6.008H70c-27.57,0-50-22.43-50-50V244c0-27.57,22.43-50,50-50h101
                                                            c5.522,0,10-4.477,10-10s-4.478-10-10-10H70c-38.598,0-70,31.402-70,70v154c0,38.598,31.402,70,70,70h59.858l41.071,41.071
                                                            c1.913,1.913,4.47,2.929,7.073,2.929c1.287,0,2.586-0.249,3.821-0.76c3.737-1.546,6.174-5.19,6.177-9.233L188.024,468H286
                                                            c38.598,0,70-31.402,70-70v-69C356,323.477,351.522,319,346,319z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M366.655,0h-25.309C261.202,0,196,65.202,196,145.346s65.202,145.345,145.345,145.345h25.309
                                                            c12.509,0,24.89-1.589,36.89-4.729l37.387,37.366c1.913,1.911,4.469,2.927,7.071,2.927c1.289,0,2.589-0.249,3.826-0.762
                                                            c3.736-1.548,6.172-5.194,6.172-9.238v-57.856c15.829-12.819,28.978-29.012,38.206-47.102
                                                            C506.687,190.751,512,168.562,512,145.346C512,65.202,446.798,0,366.655,0z M441.983,245.535
                                                            c-2.507,1.889-3.983,4.847-3.983,7.988v38.6l-24.471-24.458c-1.904-1.902-4.458-2.927-7.07-2.927c-0.98,0-1.97,0.145-2.936,0.442
                                                            c-11.903,3.658-24.307,5.512-36.868,5.512h-25.309c-69.117,0-125.346-56.23-125.346-125.346S272.23,20,341.346,20h25.309
                                                            C435.771,20,492,76.23,492,145.346C492,185.077,473.77,221.595,441.983,245.535z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M399.033,109.421c-1.443-20.935-18.319-37.811-39.255-39.254c-11.868-0.815-23.194,3.188-31.863,11.281
                                                            c-8.55,7.981-13.453,19.263-13.453,30.954c0,5.523,4.478,10,10,10c5.522,0,10-4.477,10-10c0-6.259,2.522-12.06,7.1-16.333
                                                            c4.574-4.269,10.552-6.382,16.842-5.948c11.028,0.76,19.917,9.649,20.677,20.676c0.768,11.137-6.539,20.979-17.373,23.403
                                                            c-8.778,1.964-14.908,9.592-14.908,18.549v24.025c0,5.523,4.478,10,10,10c5.523,0,10-4.477,9.999-10v-23.226
                                                            C386.949,148.68,400.468,130.242,399.033,109.421z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M363.87,209.26c-1.86-1.86-4.44-2.93-7.07-2.93s-5.21,1.07-7.07,2.93c-1.86,1.86-2.93,4.44-2.93,7.07
                                                            c0,2.64,1.071,5.22,2.93,7.08c1.86,1.86,4.44,2.92,7.07,2.92s5.21-1.06,7.07-2.92c1.86-1.87,2.93-4.44,2.93-7.08
                                                            C366.8,213.7,365.729,211.12,363.87,209.26z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M275,310H64c-5.522,0-10,4.477-10,10s4.478,10,10,10h211c5.523,0,10-4.477,10-10S280.522,310,275,310z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M282.069,368.93C280.21,367.07,277.63,366,275,366s-5.21,1.07-7.07,2.93c-1.861,1.86-2.93,4.44-2.93,7.07
                                                            s1.07,5.21,2.93,7.07c1.86,1.86,4.44,2.93,7.07,2.93s5.21-1.07,7.069-2.93c1.861-1.86,2.931-4.43,2.931-7.07
                                                            C285,373.37,283.929,370.79,282.069,368.93z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M235.667,366H64c-5.522,0-10,4.477-10,10s4.478,10,10,10h171.667c5.523,0,10-4.477,10-10S241.189,366,235.667,366z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M210,254H64c-5.522,0-10,4.477-10,10s4.478,10,10,10h146c5.523,0,10-4.477,10-10S215.522,254,210,254z"/>
                                                    </g>
                                                </g>
                                        </svg>
                                        <h4 class="title-front">Qual a vantagem em fazer parte da CleanClic?</h4>
                                    </div>
                                </div>
                                <div class="back d-flex flex-column">
                                    <div class="mt-auto">
                                        <h5 class="title-back">Qual a vantagem em fazer parte da CleanClic?</h5>
                                        <p>Adquirindo cotas de uma usina geradora de energia, será compensada na sua conta de luz a quantidade de créditos de energia relativa à quantidade de cotas. Assim, você pagará menos na sua conta de luz, economizando mensalmente.</p>                                    
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="item">
                            <div class="flip">
                                <div class="front d-flex flex-column" style="background-color: #fff;">
                                    <div class="mt-auto">
                                        <svg version="1.1" id="Capa_1" class="mb-3" width="60px" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                            viewBox="0 0 512 512" style="enable-background:new 0 0 512 512; fill: #18A074;" xml:space="preserve">
                                                <g>
                                                    <g>
                                                        <path d="M346,319c-5.522,0-10,4.477-10,10v69c0,27.57-22.43,50-50,50H178.032c-5.521,0-9.996,4.473-10,9.993l-0.014,19.882
                                                            l-23.868-23.867c-1.545-3.547-5.081-6.008-9.171-6.008H70c-27.57,0-50-22.43-50-50V244c0-27.57,22.43-50,50-50h101
                                                            c5.522,0,10-4.477,10-10s-4.478-10-10-10H70c-38.598,0-70,31.402-70,70v154c0,38.598,31.402,70,70,70h59.858l41.071,41.071
                                                            c1.913,1.913,4.47,2.929,7.073,2.929c1.287,0,2.586-0.249,3.821-0.76c3.737-1.546,6.174-5.19,6.177-9.233L188.024,468H286
                                                            c38.598,0,70-31.402,70-70v-69C356,323.477,351.522,319,346,319z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M366.655,0h-25.309C261.202,0,196,65.202,196,145.346s65.202,145.345,145.345,145.345h25.309
                                                            c12.509,0,24.89-1.589,36.89-4.729l37.387,37.366c1.913,1.911,4.469,2.927,7.071,2.927c1.289,0,2.589-0.249,3.826-0.762
                                                            c3.736-1.548,6.172-5.194,6.172-9.238v-57.856c15.829-12.819,28.978-29.012,38.206-47.102
                                                            C506.687,190.751,512,168.562,512,145.346C512,65.202,446.798,0,366.655,0z M441.983,245.535
                                                            c-2.507,1.889-3.983,4.847-3.983,7.988v38.6l-24.471-24.458c-1.904-1.902-4.458-2.927-7.07-2.927c-0.98,0-1.97,0.145-2.936,0.442
                                                            c-11.903,3.658-24.307,5.512-36.868,5.512h-25.309c-69.117,0-125.346-56.23-125.346-125.346S272.23,20,341.346,20h25.309
                                                            C435.771,20,492,76.23,492,145.346C492,185.077,473.77,221.595,441.983,245.535z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M399.033,109.421c-1.443-20.935-18.319-37.811-39.255-39.254c-11.868-0.815-23.194,3.188-31.863,11.281
                                                            c-8.55,7.981-13.453,19.263-13.453,30.954c0,5.523,4.478,10,10,10c5.522,0,10-4.477,10-10c0-6.259,2.522-12.06,7.1-16.333
                                                            c4.574-4.269,10.552-6.382,16.842-5.948c11.028,0.76,19.917,9.649,20.677,20.676c0.768,11.137-6.539,20.979-17.373,23.403
                                                            c-8.778,1.964-14.908,9.592-14.908,18.549v24.025c0,5.523,4.478,10,10,10c5.523,0,10-4.477,9.999-10v-23.226
                                                            C386.949,148.68,400.468,130.242,399.033,109.421z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M363.87,209.26c-1.86-1.86-4.44-2.93-7.07-2.93s-5.21,1.07-7.07,2.93c-1.86,1.86-2.93,4.44-2.93,7.07
                                                            c0,2.64,1.071,5.22,2.93,7.08c1.86,1.86,4.44,2.92,7.07,2.92s5.21-1.06,7.07-2.92c1.86-1.87,2.93-4.44,2.93-7.08
                                                            C366.8,213.7,365.729,211.12,363.87,209.26z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M275,310H64c-5.522,0-10,4.477-10,10s4.478,10,10,10h211c5.523,0,10-4.477,10-10S280.522,310,275,310z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M282.069,368.93C280.21,367.07,277.63,366,275,366s-5.21,1.07-7.07,2.93c-1.861,1.86-2.93,4.44-2.93,7.07
                                                            s1.07,5.21,2.93,7.07c1.86,1.86,4.44,2.93,7.07,2.93s5.21-1.07,7.069-2.93c1.861-1.86,2.931-4.43,2.931-7.07
                                                            C285,373.37,283.929,370.79,282.069,368.93z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M235.667,366H64c-5.522,0-10,4.477-10,10s4.478,10,10,10h171.667c5.523,0,10-4.477,10-10S241.189,366,235.667,366z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M210,254H64c-5.522,0-10,4.477-10,10s4.478,10,10,10h146c5.523,0,10-4.477,10-10S215.522,254,210,254z"/>
                                                    </g>
                                                </g>
                                        </svg>
                                        <h4 class="title-front">Por que eu preciso fazer parte de uma cooperativa, para ser cliente da CleanClic?</h4>
                                    </div>
                                </div>
                                <div class="back d-flex flex-column">
                                    <div class="mt-auto">
                                        <h5 class="title-back">Por que eu preciso fazer parte de uma cooperativa, para ser cliente da CleanClic?</h5>
                                        <p>Essa regra é prevista na resolução normativa nº 482, de 17 de abril de 2012, sob regulamentação da Aneel (Agência Nacional de Energia Elétrica).</p>                                    
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="item">
                            <div class="flip">
                                <div class="front d-flex flex-column" style="background-color: #fff;">
                                    <div class="mt-auto">
                                        <svg version="1.1" id="Capa_1" class="mb-3" width="60px" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                            viewBox="0 0 512 512" style="enable-background:new 0 0 512 512; fill: #18A074;" xml:space="preserve">
                                                <g>
                                                    <g>
                                                        <path d="M346,319c-5.522,0-10,4.477-10,10v69c0,27.57-22.43,50-50,50H178.032c-5.521,0-9.996,4.473-10,9.993l-0.014,19.882
                                                            l-23.868-23.867c-1.545-3.547-5.081-6.008-9.171-6.008H70c-27.57,0-50-22.43-50-50V244c0-27.57,22.43-50,50-50h101
                                                            c5.522,0,10-4.477,10-10s-4.478-10-10-10H70c-38.598,0-70,31.402-70,70v154c0,38.598,31.402,70,70,70h59.858l41.071,41.071
                                                            c1.913,1.913,4.47,2.929,7.073,2.929c1.287,0,2.586-0.249,3.821-0.76c3.737-1.546,6.174-5.19,6.177-9.233L188.024,468H286
                                                            c38.598,0,70-31.402,70-70v-69C356,323.477,351.522,319,346,319z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M366.655,0h-25.309C261.202,0,196,65.202,196,145.346s65.202,145.345,145.345,145.345h25.309
                                                            c12.509,0,24.89-1.589,36.89-4.729l37.387,37.366c1.913,1.911,4.469,2.927,7.071,2.927c1.289,0,2.589-0.249,3.826-0.762
                                                            c3.736-1.548,6.172-5.194,6.172-9.238v-57.856c15.829-12.819,28.978-29.012,38.206-47.102
                                                            C506.687,190.751,512,168.562,512,145.346C512,65.202,446.798,0,366.655,0z M441.983,245.535
                                                            c-2.507,1.889-3.983,4.847-3.983,7.988v38.6l-24.471-24.458c-1.904-1.902-4.458-2.927-7.07-2.927c-0.98,0-1.97,0.145-2.936,0.442
                                                            c-11.903,3.658-24.307,5.512-36.868,5.512h-25.309c-69.117,0-125.346-56.23-125.346-125.346S272.23,20,341.346,20h25.309
                                                            C435.771,20,492,76.23,492,145.346C492,185.077,473.77,221.595,441.983,245.535z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M399.033,109.421c-1.443-20.935-18.319-37.811-39.255-39.254c-11.868-0.815-23.194,3.188-31.863,11.281
                                                            c-8.55,7.981-13.453,19.263-13.453,30.954c0,5.523,4.478,10,10,10c5.522,0,10-4.477,10-10c0-6.259,2.522-12.06,7.1-16.333
                                                            c4.574-4.269,10.552-6.382,16.842-5.948c11.028,0.76,19.917,9.649,20.677,20.676c0.768,11.137-6.539,20.979-17.373,23.403
                                                            c-8.778,1.964-14.908,9.592-14.908,18.549v24.025c0,5.523,4.478,10,10,10c5.523,0,10-4.477,9.999-10v-23.226
                                                            C386.949,148.68,400.468,130.242,399.033,109.421z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M363.87,209.26c-1.86-1.86-4.44-2.93-7.07-2.93s-5.21,1.07-7.07,2.93c-1.86,1.86-2.93,4.44-2.93,7.07
                                                            c0,2.64,1.071,5.22,2.93,7.08c1.86,1.86,4.44,2.92,7.07,2.92s5.21-1.06,7.07-2.92c1.86-1.87,2.93-4.44,2.93-7.08
                                                            C366.8,213.7,365.729,211.12,363.87,209.26z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M275,310H64c-5.522,0-10,4.477-10,10s4.478,10,10,10h211c5.523,0,10-4.477,10-10S280.522,310,275,310z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M282.069,368.93C280.21,367.07,277.63,366,275,366s-5.21,1.07-7.07,2.93c-1.861,1.86-2.93,4.44-2.93,7.07
                                                            s1.07,5.21,2.93,7.07c1.86,1.86,4.44,2.93,7.07,2.93s5.21-1.07,7.069-2.93c1.861-1.86,2.931-4.43,2.931-7.07
                                                            C285,373.37,283.929,370.79,282.069,368.93z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M235.667,366H64c-5.522,0-10,4.477-10,10s4.478,10,10,10h171.667c5.523,0,10-4.477,10-10S241.189,366,235.667,366z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M210,254H64c-5.522,0-10,4.477-10,10s4.478,10,10,10h146c5.523,0,10-4.477,10-10S215.522,254,210,254z"/>
                                                    </g>
                                                </g>
                                        </svg>
                                        <h4 class="title-front">O que eu preciso para me tornar um cliente da CleanClic ?</h4>
                                    </div>
                                </div>
                                <div class="back d-flex flex-column">
                                    <div class="mt-auto">
                                        <h5 class="title-back">O que eu preciso para me tornar um cliente da CleanClic ?</h5>
                                        <p>É simples! Você precisa:
                                            1. Ser titular da sua conta de luz e ter um cartão de crédito que possa usar;
                                            2. Fazer uma conta na CleanClic;
                                            3. Ser um cooperado Sicoob e integralizar uma cota-parte de R$ 10,00;
                                            4. Adquirir cotas de uma usina.
                                            Para isso, é preciso que tenhamos cotas disponíveis nas nossas usinas. Caso essas cotas não estejam livres quando você se juntar a nós, não desanime! Em breve entraremos em contato com você.
                                            Depois disso, é só aproveitar a economia!
                                        </p>                                    
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="item">
                            <div class="flip">
                                <div class="front d-flex flex-column" style="background-color: #fff;">
                                    <div class="mt-auto">
                                        <svg version="1.1" id="Capa_1" class="mb-3" width="60px" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                            viewBox="0 0 512 512" style="enable-background:new 0 0 512 512; fill: #18A074;" xml:space="preserve">
                                                <g>
                                                    <g>
                                                        <path d="M346,319c-5.522,0-10,4.477-10,10v69c0,27.57-22.43,50-50,50H178.032c-5.521,0-9.996,4.473-10,9.993l-0.014,19.882
                                                            l-23.868-23.867c-1.545-3.547-5.081-6.008-9.171-6.008H70c-27.57,0-50-22.43-50-50V244c0-27.57,22.43-50,50-50h101
                                                            c5.522,0,10-4.477,10-10s-4.478-10-10-10H70c-38.598,0-70,31.402-70,70v154c0,38.598,31.402,70,70,70h59.858l41.071,41.071
                                                            c1.913,1.913,4.47,2.929,7.073,2.929c1.287,0,2.586-0.249,3.821-0.76c3.737-1.546,6.174-5.19,6.177-9.233L188.024,468H286
                                                            c38.598,0,70-31.402,70-70v-69C356,323.477,351.522,319,346,319z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M366.655,0h-25.309C261.202,0,196,65.202,196,145.346s65.202,145.345,145.345,145.345h25.309
                                                            c12.509,0,24.89-1.589,36.89-4.729l37.387,37.366c1.913,1.911,4.469,2.927,7.071,2.927c1.289,0,2.589-0.249,3.826-0.762
                                                            c3.736-1.548,6.172-5.194,6.172-9.238v-57.856c15.829-12.819,28.978-29.012,38.206-47.102
                                                            C506.687,190.751,512,168.562,512,145.346C512,65.202,446.798,0,366.655,0z M441.983,245.535
                                                            c-2.507,1.889-3.983,4.847-3.983,7.988v38.6l-24.471-24.458c-1.904-1.902-4.458-2.927-7.07-2.927c-0.98,0-1.97,0.145-2.936,0.442
                                                            c-11.903,3.658-24.307,5.512-36.868,5.512h-25.309c-69.117,0-125.346-56.23-125.346-125.346S272.23,20,341.346,20h25.309
                                                            C435.771,20,492,76.23,492,145.346C492,185.077,473.77,221.595,441.983,245.535z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M399.033,109.421c-1.443-20.935-18.319-37.811-39.255-39.254c-11.868-0.815-23.194,3.188-31.863,11.281
                                                            c-8.55,7.981-13.453,19.263-13.453,30.954c0,5.523,4.478,10,10,10c5.522,0,10-4.477,10-10c0-6.259,2.522-12.06,7.1-16.333
                                                            c4.574-4.269,10.552-6.382,16.842-5.948c11.028,0.76,19.917,9.649,20.677,20.676c0.768,11.137-6.539,20.979-17.373,23.403
                                                            c-8.778,1.964-14.908,9.592-14.908,18.549v24.025c0,5.523,4.478,10,10,10c5.523,0,10-4.477,9.999-10v-23.226
                                                            C386.949,148.68,400.468,130.242,399.033,109.421z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M363.87,209.26c-1.86-1.86-4.44-2.93-7.07-2.93s-5.21,1.07-7.07,2.93c-1.86,1.86-2.93,4.44-2.93,7.07
                                                            c0,2.64,1.071,5.22,2.93,7.08c1.86,1.86,4.44,2.92,7.07,2.92s5.21-1.06,7.07-2.92c1.86-1.87,2.93-4.44,2.93-7.08
                                                            C366.8,213.7,365.729,211.12,363.87,209.26z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M275,310H64c-5.522,0-10,4.477-10,10s4.478,10,10,10h211c5.523,0,10-4.477,10-10S280.522,310,275,310z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M282.069,368.93C280.21,367.07,277.63,366,275,366s-5.21,1.07-7.07,2.93c-1.861,1.86-2.93,4.44-2.93,7.07
                                                            s1.07,5.21,2.93,7.07c1.86,1.86,4.44,2.93,7.07,2.93s5.21-1.07,7.069-2.93c1.861-1.86,2.931-4.43,2.931-7.07
                                                            C285,373.37,283.929,370.79,282.069,368.93z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M235.667,366H64c-5.522,0-10,4.477-10,10s4.478,10,10,10h171.667c5.523,0,10-4.477,10-10S241.189,366,235.667,366z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M210,254H64c-5.522,0-10,4.477-10,10s4.478,10,10,10h146c5.523,0,10-4.477,10-10S215.522,254,210,254z"/>
                                                    </g>
                                                </g>
                                        </svg>
                                        <h4 class="title-front">Após me tornar cliente CleanClic, dentro de quanto tempo irei usufruir do desconto na conta de luz ?</h4>
                                    </div>
                                </div>
                                <div class="back d-flex flex-column">
                                    <div class="mt-auto">
                                        <h5 class="title-back">Após me tornar cliente CleanClic, dentro de quanto tempo irei usufruir do desconto na conta de luz ?</h5>
                                        <p>De acordo com a Aneel, os dados devem ser passados para a concessionária por escrito com antecedência de 60 dias. Ou seja, após se tornar cliente CleanClic, você usufruirá do desconto após até 60 dias.
                                            Você só passará a pagar a mensalidade da CleanClic após começar a usufruir do desconto na sua conta de luz, e toda a comunicação com a concessionária é por conta da Cleanclic!
                                            O prazo de até 60 dias também vale para cancelamento e alteração do plano (quantidade de cotas adquiridas).
                                        </p>                                    
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="item">
                            <div class="flip">
                                <div class="front d-flex flex-column" style="background-color: #fff;">
                                    <div class="mt-auto">
                                        <svg version="1.1" id="Capa_1" class="mb-3" width="60px" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                            viewBox="0 0 512 512" style="enable-background:new 0 0 512 512; fill: #18A074;" xml:space="preserve">
                                                <g>
                                                    <g>
                                                        <path d="M346,319c-5.522,0-10,4.477-10,10v69c0,27.57-22.43,50-50,50H178.032c-5.521,0-9.996,4.473-10,9.993l-0.014,19.882
                                                            l-23.868-23.867c-1.545-3.547-5.081-6.008-9.171-6.008H70c-27.57,0-50-22.43-50-50V244c0-27.57,22.43-50,50-50h101
                                                            c5.522,0,10-4.477,10-10s-4.478-10-10-10H70c-38.598,0-70,31.402-70,70v154c0,38.598,31.402,70,70,70h59.858l41.071,41.071
                                                            c1.913,1.913,4.47,2.929,7.073,2.929c1.287,0,2.586-0.249,3.821-0.76c3.737-1.546,6.174-5.19,6.177-9.233L188.024,468H286
                                                            c38.598,0,70-31.402,70-70v-69C356,323.477,351.522,319,346,319z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M366.655,0h-25.309C261.202,0,196,65.202,196,145.346s65.202,145.345,145.345,145.345h25.309
                                                            c12.509,0,24.89-1.589,36.89-4.729l37.387,37.366c1.913,1.911,4.469,2.927,7.071,2.927c1.289,0,2.589-0.249,3.826-0.762
                                                            c3.736-1.548,6.172-5.194,6.172-9.238v-57.856c15.829-12.819,28.978-29.012,38.206-47.102
                                                            C506.687,190.751,512,168.562,512,145.346C512,65.202,446.798,0,366.655,0z M441.983,245.535
                                                            c-2.507,1.889-3.983,4.847-3.983,7.988v38.6l-24.471-24.458c-1.904-1.902-4.458-2.927-7.07-2.927c-0.98,0-1.97,0.145-2.936,0.442
                                                            c-11.903,3.658-24.307,5.512-36.868,5.512h-25.309c-69.117,0-125.346-56.23-125.346-125.346S272.23,20,341.346,20h25.309
                                                            C435.771,20,492,76.23,492,145.346C492,185.077,473.77,221.595,441.983,245.535z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M399.033,109.421c-1.443-20.935-18.319-37.811-39.255-39.254c-11.868-0.815-23.194,3.188-31.863,11.281
                                                            c-8.55,7.981-13.453,19.263-13.453,30.954c0,5.523,4.478,10,10,10c5.522,0,10-4.477,10-10c0-6.259,2.522-12.06,7.1-16.333
                                                            c4.574-4.269,10.552-6.382,16.842-5.948c11.028,0.76,19.917,9.649,20.677,20.676c0.768,11.137-6.539,20.979-17.373,23.403
                                                            c-8.778,1.964-14.908,9.592-14.908,18.549v24.025c0,5.523,4.478,10,10,10c5.523,0,10-4.477,9.999-10v-23.226
                                                            C386.949,148.68,400.468,130.242,399.033,109.421z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M363.87,209.26c-1.86-1.86-4.44-2.93-7.07-2.93s-5.21,1.07-7.07,2.93c-1.86,1.86-2.93,4.44-2.93,7.07
                                                            c0,2.64,1.071,5.22,2.93,7.08c1.86,1.86,4.44,2.92,7.07,2.92s5.21-1.06,7.07-2.92c1.86-1.87,2.93-4.44,2.93-7.08
                                                            C366.8,213.7,365.729,211.12,363.87,209.26z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M275,310H64c-5.522,0-10,4.477-10,10s4.478,10,10,10h211c5.523,0,10-4.477,10-10S280.522,310,275,310z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M282.069,368.93C280.21,367.07,277.63,366,275,366s-5.21,1.07-7.07,2.93c-1.861,1.86-2.93,4.44-2.93,7.07
                                                            s1.07,5.21,2.93,7.07c1.86,1.86,4.44,2.93,7.07,2.93s5.21-1.07,7.069-2.93c1.861-1.86,2.931-4.43,2.931-7.07
                                                            C285,373.37,283.929,370.79,282.069,368.93z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M235.667,366H64c-5.522,0-10,4.477-10,10s4.478,10,10,10h171.667c5.523,0,10-4.477,10-10S241.189,366,235.667,366z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M210,254H64c-5.522,0-10,4.477-10,10s4.478,10,10,10h146c5.523,0,10-4.477,10-10S215.522,254,210,254z"/>
                                                    </g>
                                                </g>
                                        </svg>
                                        <h4 class="title-front">É necessária a instalação de placas solares no meu imóvel ?</h4>
                                    </div>
                                </div>
                                <div class="back d-flex flex-column">
                                    <div class="mt-auto">
                                        <h5 class="title-back">É necessária a instalação de placas solares no meu imóvel ?</h5>
                                        <p>Não! No modelo de geração compartilhada de energia, você pode usufruir de descontos mensais na sua conta de luz sem a necessidade de instalação de placas na sua residência.
                                        </p>                                    
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="item">
                            <div class="flip">
                                <div class="front d-flex flex-column" style="background-color: #fff;">
                                    <div class="mt-auto">
                                        <svg version="1.1" id="Capa_1" class="mb-3" width="60px" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                            viewBox="0 0 512 512" style="enable-background:new 0 0 512 512; fill: #18A074;" xml:space="preserve">
                                                <g>
                                                    <g>
                                                        <path d="M346,319c-5.522,0-10,4.477-10,10v69c0,27.57-22.43,50-50,50H178.032c-5.521,0-9.996,4.473-10,9.993l-0.014,19.882
                                                            l-23.868-23.867c-1.545-3.547-5.081-6.008-9.171-6.008H70c-27.57,0-50-22.43-50-50V244c0-27.57,22.43-50,50-50h101
                                                            c5.522,0,10-4.477,10-10s-4.478-10-10-10H70c-38.598,0-70,31.402-70,70v154c0,38.598,31.402,70,70,70h59.858l41.071,41.071
                                                            c1.913,1.913,4.47,2.929,7.073,2.929c1.287,0,2.586-0.249,3.821-0.76c3.737-1.546,6.174-5.19,6.177-9.233L188.024,468H286
                                                            c38.598,0,70-31.402,70-70v-69C356,323.477,351.522,319,346,319z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M366.655,0h-25.309C261.202,0,196,65.202,196,145.346s65.202,145.345,145.345,145.345h25.309
                                                            c12.509,0,24.89-1.589,36.89-4.729l37.387,37.366c1.913,1.911,4.469,2.927,7.071,2.927c1.289,0,2.589-0.249,3.826-0.762
                                                            c3.736-1.548,6.172-5.194,6.172-9.238v-57.856c15.829-12.819,28.978-29.012,38.206-47.102
                                                            C506.687,190.751,512,168.562,512,145.346C512,65.202,446.798,0,366.655,0z M441.983,245.535
                                                            c-2.507,1.889-3.983,4.847-3.983,7.988v38.6l-24.471-24.458c-1.904-1.902-4.458-2.927-7.07-2.927c-0.98,0-1.97,0.145-2.936,0.442
                                                            c-11.903,3.658-24.307,5.512-36.868,5.512h-25.309c-69.117,0-125.346-56.23-125.346-125.346S272.23,20,341.346,20h25.309
                                                            C435.771,20,492,76.23,492,145.346C492,185.077,473.77,221.595,441.983,245.535z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M399.033,109.421c-1.443-20.935-18.319-37.811-39.255-39.254c-11.868-0.815-23.194,3.188-31.863,11.281
                                                            c-8.55,7.981-13.453,19.263-13.453,30.954c0,5.523,4.478,10,10,10c5.522,0,10-4.477,10-10c0-6.259,2.522-12.06,7.1-16.333
                                                            c4.574-4.269,10.552-6.382,16.842-5.948c11.028,0.76,19.917,9.649,20.677,20.676c0.768,11.137-6.539,20.979-17.373,23.403
                                                            c-8.778,1.964-14.908,9.592-14.908,18.549v24.025c0,5.523,4.478,10,10,10c5.523,0,10-4.477,9.999-10v-23.226
                                                            C386.949,148.68,400.468,130.242,399.033,109.421z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M363.87,209.26c-1.86-1.86-4.44-2.93-7.07-2.93s-5.21,1.07-7.07,2.93c-1.86,1.86-2.93,4.44-2.93,7.07
                                                            c0,2.64,1.071,5.22,2.93,7.08c1.86,1.86,4.44,2.92,7.07,2.92s5.21-1.06,7.07-2.92c1.86-1.87,2.93-4.44,2.93-7.08
                                                            C366.8,213.7,365.729,211.12,363.87,209.26z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M275,310H64c-5.522,0-10,4.477-10,10s4.478,10,10,10h211c5.523,0,10-4.477,10-10S280.522,310,275,310z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M282.069,368.93C280.21,367.07,277.63,366,275,366s-5.21,1.07-7.07,2.93c-1.861,1.86-2.93,4.44-2.93,7.07
                                                            s1.07,5.21,2.93,7.07c1.86,1.86,4.44,2.93,7.07,2.93s5.21-1.07,7.069-2.93c1.861-1.86,2.931-4.43,2.931-7.07
                                                            C285,373.37,283.929,370.79,282.069,368.93z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M235.667,366H64c-5.522,0-10,4.477-10,10s4.478,10,10,10h171.667c5.523,0,10-4.477,10-10S241.189,366,235.667,366z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M210,254H64c-5.522,0-10,4.477-10,10s4.478,10,10,10h146c5.523,0,10-4.477,10-10S215.522,254,210,254z"/>
                                                    </g>
                                                </g>
                                        </svg>
                                        <h4 class="title-front">Para participar, eu devo ser o titular da minha conta de luz ?</h4>
                                    </div>
                                </div>
                                <div class="back d-flex flex-column">
                                    <div class="mt-auto">
                                        <h5 class="title-back">Para participar, eu devo ser o titular da minha conta de luz ?</h5>
                                        <p>Sim Se você registrar os seus dados na conta da CleanClic, você deve também fazer parte de uma cooperativa e ser titular da sua conta de luz.
                                            Se você não for o titular, você pode solicitar a alteração de titularidade junto à concessionária, isto normalmente leva em torno de dois dias.
                                        </p>                                    
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="item">
                            <div class="flip">
                                <div class="front d-flex flex-column" style="background-color: #fff;">
                                    <div class="mt-auto">
                                        <svg version="1.1" id="Capa_1" class="mb-3" width="60px" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                            viewBox="0 0 512 512" style="enable-background:new 0 0 512 512; fill: #18A074;" xml:space="preserve">
                                                <g>
                                                    <g>
                                                        <path d="M346,319c-5.522,0-10,4.477-10,10v69c0,27.57-22.43,50-50,50H178.032c-5.521,0-9.996,4.473-10,9.993l-0.014,19.882
                                                            l-23.868-23.867c-1.545-3.547-5.081-6.008-9.171-6.008H70c-27.57,0-50-22.43-50-50V244c0-27.57,22.43-50,50-50h101
                                                            c5.522,0,10-4.477,10-10s-4.478-10-10-10H70c-38.598,0-70,31.402-70,70v154c0,38.598,31.402,70,70,70h59.858l41.071,41.071
                                                            c1.913,1.913,4.47,2.929,7.073,2.929c1.287,0,2.586-0.249,3.821-0.76c3.737-1.546,6.174-5.19,6.177-9.233L188.024,468H286
                                                            c38.598,0,70-31.402,70-70v-69C356,323.477,351.522,319,346,319z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M366.655,0h-25.309C261.202,0,196,65.202,196,145.346s65.202,145.345,145.345,145.345h25.309
                                                            c12.509,0,24.89-1.589,36.89-4.729l37.387,37.366c1.913,1.911,4.469,2.927,7.071,2.927c1.289,0,2.589-0.249,3.826-0.762
                                                            c3.736-1.548,6.172-5.194,6.172-9.238v-57.856c15.829-12.819,28.978-29.012,38.206-47.102
                                                            C506.687,190.751,512,168.562,512,145.346C512,65.202,446.798,0,366.655,0z M441.983,245.535
                                                            c-2.507,1.889-3.983,4.847-3.983,7.988v38.6l-24.471-24.458c-1.904-1.902-4.458-2.927-7.07-2.927c-0.98,0-1.97,0.145-2.936,0.442
                                                            c-11.903,3.658-24.307,5.512-36.868,5.512h-25.309c-69.117,0-125.346-56.23-125.346-125.346S272.23,20,341.346,20h25.309
                                                            C435.771,20,492,76.23,492,145.346C492,185.077,473.77,221.595,441.983,245.535z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M399.033,109.421c-1.443-20.935-18.319-37.811-39.255-39.254c-11.868-0.815-23.194,3.188-31.863,11.281
                                                            c-8.55,7.981-13.453,19.263-13.453,30.954c0,5.523,4.478,10,10,10c5.522,0,10-4.477,10-10c0-6.259,2.522-12.06,7.1-16.333
                                                            c4.574-4.269,10.552-6.382,16.842-5.948c11.028,0.76,19.917,9.649,20.677,20.676c0.768,11.137-6.539,20.979-17.373,23.403
                                                            c-8.778,1.964-14.908,9.592-14.908,18.549v24.025c0,5.523,4.478,10,10,10c5.523,0,10-4.477,9.999-10v-23.226
                                                            C386.949,148.68,400.468,130.242,399.033,109.421z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M363.87,209.26c-1.86-1.86-4.44-2.93-7.07-2.93s-5.21,1.07-7.07,2.93c-1.86,1.86-2.93,4.44-2.93,7.07
                                                            c0,2.64,1.071,5.22,2.93,7.08c1.86,1.86,4.44,2.92,7.07,2.92s5.21-1.06,7.07-2.92c1.86-1.87,2.93-4.44,2.93-7.08
                                                            C366.8,213.7,365.729,211.12,363.87,209.26z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M275,310H64c-5.522,0-10,4.477-10,10s4.478,10,10,10h211c5.523,0,10-4.477,10-10S280.522,310,275,310z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M282.069,368.93C280.21,367.07,277.63,366,275,366s-5.21,1.07-7.07,2.93c-1.861,1.86-2.93,4.44-2.93,7.07
                                                            s1.07,5.21,2.93,7.07c1.86,1.86,4.44,2.93,7.07,2.93s5.21-1.07,7.069-2.93c1.861-1.86,2.931-4.43,2.931-7.07
                                                            C285,373.37,283.929,370.79,282.069,368.93z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M235.667,366H64c-5.522,0-10,4.477-10,10s4.478,10,10,10h171.667c5.523,0,10-4.477,10-10S241.189,366,235.667,366z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M210,254H64c-5.522,0-10,4.477-10,10s4.478,10,10,10h146c5.523,0,10-4.477,10-10S215.522,254,210,254z"/>
                                                    </g>
                                                </g>
                                        </svg>
                                        <h4 class="title-front">Qual é o papel da minha concessionária, se eu me tornar um cliente CleanClic ?</h4>
                                    </div>
                                </div>
                                <div class="back d-flex flex-column">
                                    <div class="mt-auto">
                                        <h5 class="title-back">Qual é o papel da minha concessionária, se eu me tornar um cliente CleanClic ?</h5>
                                        <p>Você continuará consumindo a energia que vem da concessionária. Independente de quantos créditos de energia as suas cotas proporcionem, você sempre terá que pagar, pelo menos, o custo de disponibilidade cobrado em sua conta de luz - que é a taxa mínima exigida pela concessionária.
                                        </p>                                    
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="item">
                            <div class="flip">
                                <div class="front d-flex flex-column" style="background-color: #fff;">
                                    <div class="mt-auto">
                                        <svg version="1.1" id="Capa_1" class="mb-3" width="60px" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                            viewBox="0 0 512 512" style="enable-background:new 0 0 512 512; fill: #18A074;" xml:space="preserve">
                                                <g>
                                                    <g>
                                                        <path d="M346,319c-5.522,0-10,4.477-10,10v69c0,27.57-22.43,50-50,50H178.032c-5.521,0-9.996,4.473-10,9.993l-0.014,19.882
                                                            l-23.868-23.867c-1.545-3.547-5.081-6.008-9.171-6.008H70c-27.57,0-50-22.43-50-50V244c0-27.57,22.43-50,50-50h101
                                                            c5.522,0,10-4.477,10-10s-4.478-10-10-10H70c-38.598,0-70,31.402-70,70v154c0,38.598,31.402,70,70,70h59.858l41.071,41.071
                                                            c1.913,1.913,4.47,2.929,7.073,2.929c1.287,0,2.586-0.249,3.821-0.76c3.737-1.546,6.174-5.19,6.177-9.233L188.024,468H286
                                                            c38.598,0,70-31.402,70-70v-69C356,323.477,351.522,319,346,319z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M366.655,0h-25.309C261.202,0,196,65.202,196,145.346s65.202,145.345,145.345,145.345h25.309
                                                            c12.509,0,24.89-1.589,36.89-4.729l37.387,37.366c1.913,1.911,4.469,2.927,7.071,2.927c1.289,0,2.589-0.249,3.826-0.762
                                                            c3.736-1.548,6.172-5.194,6.172-9.238v-57.856c15.829-12.819,28.978-29.012,38.206-47.102
                                                            C506.687,190.751,512,168.562,512,145.346C512,65.202,446.798,0,366.655,0z M441.983,245.535
                                                            c-2.507,1.889-3.983,4.847-3.983,7.988v38.6l-24.471-24.458c-1.904-1.902-4.458-2.927-7.07-2.927c-0.98,0-1.97,0.145-2.936,0.442
                                                            c-11.903,3.658-24.307,5.512-36.868,5.512h-25.309c-69.117,0-125.346-56.23-125.346-125.346S272.23,20,341.346,20h25.309
                                                            C435.771,20,492,76.23,492,145.346C492,185.077,473.77,221.595,441.983,245.535z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M399.033,109.421c-1.443-20.935-18.319-37.811-39.255-39.254c-11.868-0.815-23.194,3.188-31.863,11.281
                                                            c-8.55,7.981-13.453,19.263-13.453,30.954c0,5.523,4.478,10,10,10c5.522,0,10-4.477,10-10c0-6.259,2.522-12.06,7.1-16.333
                                                            c4.574-4.269,10.552-6.382,16.842-5.948c11.028,0.76,19.917,9.649,20.677,20.676c0.768,11.137-6.539,20.979-17.373,23.403
                                                            c-8.778,1.964-14.908,9.592-14.908,18.549v24.025c0,5.523,4.478,10,10,10c5.523,0,10-4.477,9.999-10v-23.226
                                                            C386.949,148.68,400.468,130.242,399.033,109.421z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M363.87,209.26c-1.86-1.86-4.44-2.93-7.07-2.93s-5.21,1.07-7.07,2.93c-1.86,1.86-2.93,4.44-2.93,7.07
                                                            c0,2.64,1.071,5.22,2.93,7.08c1.86,1.86,4.44,2.92,7.07,2.92s5.21-1.06,7.07-2.92c1.86-1.87,2.93-4.44,2.93-7.08
                                                            C366.8,213.7,365.729,211.12,363.87,209.26z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M275,310H64c-5.522,0-10,4.477-10,10s4.478,10,10,10h211c5.523,0,10-4.477,10-10S280.522,310,275,310z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M282.069,368.93C280.21,367.07,277.63,366,275,366s-5.21,1.07-7.07,2.93c-1.861,1.86-2.93,4.44-2.93,7.07
                                                            s1.07,5.21,2.93,7.07c1.86,1.86,4.44,2.93,7.07,2.93s5.21-1.07,7.069-2.93c1.861-1.86,2.931-4.43,2.931-7.07
                                                            C285,373.37,283.929,370.79,282.069,368.93z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M235.667,366H64c-5.522,0-10,4.477-10,10s4.478,10,10,10h171.667c5.523,0,10-4.477,10-10S241.189,366,235.667,366z"/>
                                                    </g>
                                                </g>
                                                <g>
                                                    <g>
                                                        <path d="M210,254H64c-5.522,0-10,4.477-10,10s4.478,10,10,10h146c5.523,0,10-4.477,10-10S215.522,254,210,254z"/>
                                                    </g>
                                                </g>
                                        </svg>
                                        <h4 class="title-front">Nos meses de chuva, o meu desconto diminuiu?</h4>
                                    </div>
                                </div>
                                <div class="back d-flex flex-column">
                                    <div class="mt-auto">
                                        <h5 class="title-back">Nos meses de chuva, o meu desconto diminuiu?</h5>
                                        <p>A geração de energia elétrica depende da captação da energia solar, portanto, essa geração varia mensalmente, e nos meses de chuva o desconto pode ser menor.
                                        </p>                                    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="parthness" id="parceiros">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center wow fadeInDown" data-wow-duration="1s" data-wow-delay=".5s">
                    <h1 class="title mb-4">Conheça<br><span class="bold-title">nossos parceiros</span></h1>
                </div>
            </div>

            <div class="row">
                <div class="col-6 col-lg-3 wow fadeInDown" data-wow-duration="1s" data-wow-delay=".6s">
                    <a href="#" target="_BLANK">
                        <img src="assets/images/parceiros/01.png" class="img-fluid" />
                    </a>
                </div>

                <div class="col-6 col-lg-3 wow fadeInDown" data-wow-duration="1s" data-wow-delay=".7s">
                    <a href="#" target="_BLANK">
                        <img src="assets/images/parceiros/02.png" class="img-fluid" />
                    </a>
                </div>

                <div class="col-6 col-lg-3 wow fadeInDown" data-wow-duration="1s" data-wow-delay=".8s">
                    <a href="#" target="_BLANK">
                        <img src="assets/images/parceiros/03.png" class="img-fluid" />
                    </a>
                </div>

                <div class="col-6 col-lg-3 wow fadeInDown" data-wow-duration="1s" data-wow-delay=".9s">
                    <a href="#" target="_BLANK">
                        <img src="assets/images/parceiros/04.png" class="img-fluid" />
                    </a>
                </div>
            </div>
        </div>
    </section>

    <section class="contact wow fadeInUp" data-wow-duration="1s" data-wow-delay=".1s" id="contato">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 offset-lg-1 wow fadeInUp" data-wow-duration="1s" data-wow-delay=".7s">
                    <h1 class="title">Ainda tem dúvidas ?<br><span class="bold-title">entre em contato conosco</span></h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-10 offset-lg-1 wow fadeInUp" data-wow-duration="1s" data-wow-delay=".8s">
                    <form id="contact-form">
                        <div class="form-row">
                            <div class="form-group input-material col-lg-6">
                                <input type="text" class="form-control" id="name-field" required>
                                <label for="name-field">Digite seu nome</label>
                            </div>
                            <div class="form-group input-material col-lg-6">
                                <input type="email" class="form-control" id="email-field" required>
                                <label for="email-field">Informe seu e-mail</label>
                            </div>
                            <div class="form-group input-material col-lg-6">
                                <input type="text" class="form-control" id="name-field" required>
                                <label for="name-field">Qual o seu telefone?</label>
                            </div>
                            <div class="form-group input-material col-lg-6">
                                <input type="email" class="form-control" id="email-field" required>
                                <label for="email-field">Assunto</label>
                            </div>
                            <div class="form-group input-material col-lg-12">
                                <textarea class="form-control" id="textarea-field" rows="3" required></textarea>
                                <label for="textarea-field">Sua mensagem</label>
                            </div>
                            <div class="form-group col-lg-12 send mt-4">
                                <a href="#." class="cta-2 mt-3 button" id="send-contact" data-dismiss="modal">
                                    <span>Enviar mensagem</span>
                                    <svg width="13px" height="10px" viewBox="0 0 13 10">
                                        <path d="M1,5 L11,5"></path>
                                        <polyline points="8 1 12 5 8 9"></polyline>
                                    </svg>
                                </a>
                            </div>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="row footer">
                    <div class="col-lg-6 align-self-center social-network">
                        <a href="#." target="_BLANK">
                            <svg height="30px" viewBox="0 0 512 512" width="30px" style="fill: #fff;" xmlns="http://www.w3.org/2000/svg">
                                <path d="m297.277344 508.667969c-2.132813.347656-4.273438.667969-6.421875.960937 2.148437-.292968 4.289062-.613281 6.421875-.960937zm0 0"/><path d="m302.398438 507.792969c-1.019532.1875-2.039063.359375-3.058594.535156 1.019531-.175781 2.039062-.347656 3.058594-.535156zm0 0"/><path d="m285.136719 510.339844c-2.496094.28125-5.007813.53125-7.527344.742187 2.519531-.210937 5.03125-.460937 7.527344-.742187zm0 0"/><path d="m290.054688 509.738281c-1.199219.160157-2.40625.308594-3.609376.449219 1.203126-.140625 2.410157-.289062 3.609376-.449219zm0 0"/><path d="m309.367188 506.410156c-.898438.191406-1.800782.382813-2.703126.566406.902344-.183593 1.804688-.375 2.703126-.566406zm0 0"/><path d="m326.664062 502.113281c-.726562.207031-1.453124.402344-2.179687.605469.726563-.203125 1.453125-.398438 2.179687-.605469zm0 0"/><path d="m321.433594 503.542969c-.789063.207031-1.582032.417969-2.375.617187.792968-.199218 1.585937-.40625 2.375-.617187zm0 0"/><path d="m314.589844 505.253906c-.835938.195313-1.679688.378906-2.523438.566406.84375-.1875 1.6875-.371093 2.523438-.566406zm0 0"/><path d="m277.527344 511.089844c-1.347656.113281-2.695313.214844-4.046875.304687 1.351562-.089843 2.699219-.191406 4.046875-.304687zm0 0"/><path d="m512 256c0-141.363281-114.636719-256-256-256s-256 114.636719-256 256 114.636719 256 256 256c1.503906 0 3-.03125 4.5-.058594v-199.285156h-55v-64.097656h55v-47.167969c0-54.703125 33.394531-84.476563 82.191406-84.476563 23.367188 0 43.453125 1.742188 49.308594 2.519532v57.171875h-33.648438c-26.546874 0-31.6875 12.617187-31.6875 31.128906v40.824219h63.476563l-8.273437 64.097656h-55.203126v189.453125c107.003907-30.675781 185.335938-129.257813 185.335938-246.109375zm0 0"/><path d="m272.914062 511.429688c-2.664062.171874-5.339843.308593-8.023437.398437 2.683594-.089844 5.359375-.226563 8.023437-.398437zm0 0"/>
                                <path d="m264.753906 511.835938c-1.414062.046874-2.832031.082031-4.25.105468 1.417969-.023437 2.835938-.058594 4.25-.105468zm0 0"/>
                            </svg>
                        </a>

                        <a href="https://www.facebook.com/cleanclic/" target="_BLANK">
                            <svg height="30px" viewBox="0 0 512 512" width="30px" style="fill: #fff;" xmlns="http://www.w3.org/2000/svg"><path d="m305 256c0 27.0625-21.9375 49-49 49s-49-21.9375-49-49 21.9375-49 49-49 49 21.9375 49 49zm0 0"/><path d="m370.59375 169.304688c-2.355469-6.382813-6.113281-12.160157-10.996094-16.902344-4.742187-4.882813-10.515625-8.640625-16.902344-10.996094-5.179687-2.011719-12.960937-4.40625-27.292968-5.058594-15.503906-.707031-20.152344-.859375-59.402344-.859375-39.253906 0-43.902344.148438-59.402344.855469-14.332031.65625-22.117187 3.050781-27.292968 5.0625-6.386719 2.355469-12.164063 6.113281-16.902344 10.996094-4.882813 4.742187-8.640625 10.515625-11 16.902344-2.011719 5.179687-4.40625 12.964843-5.058594 27.296874-.707031 15.5-.859375 20.148438-.859375 59.402344 0 39.25.152344 43.898438.859375 59.402344.652344 14.332031 3.046875 22.113281 5.058594 27.292969 2.359375 6.386719 6.113281 12.160156 10.996094 16.902343 4.742187 4.882813 10.515624 8.640626 16.902343 10.996094 5.179688 2.015625 12.964844 4.410156 27.296875 5.0625 15.5.707032 20.144532.855469 59.398438.855469 39.257812 0 43.90625-.148437 59.402344-.855469 14.332031-.652344 22.117187-3.046875 27.296874-5.0625 12.820313-4.945312 22.953126-15.078125 27.898438-27.898437 2.011719-5.179688 4.40625-12.960938 5.0625-27.292969.707031-15.503906.855469-20.152344.855469-59.402344 0-39.253906-.148438-43.902344-.855469-59.402344-.652344-14.332031-3.046875-22.117187-5.0625-27.296874zm-114.59375 162.179687c-41.691406 0-75.488281-33.792969-75.488281-75.484375s33.796875-75.484375 75.488281-75.484375c41.6875 0 75.484375 33.792969 75.484375 75.484375s-33.796875 75.484375-75.484375 75.484375zm78.46875-136.3125c-9.742188 0-17.640625-7.898437-17.640625-17.640625s7.898437-17.640625 17.640625-17.640625 17.640625 7.898437 17.640625 17.640625c-.003906 9.742188-7.898437 17.640625-17.640625 17.640625zm0 0"/><path d="m256 0c-141.363281 0-256 114.636719-256 256s114.636719 256 256 256 256-114.636719 256-256-114.636719-256-256-256zm146.113281 316.605469c-.710937 15.648437-3.199219 26.332031-6.832031 35.683593-7.636719 19.746094-23.246094 35.355469-42.992188 42.992188-9.347656 3.632812-20.035156 6.117188-35.679687 6.832031-15.675781.714844-20.683594.886719-60.605469.886719-39.925781 0-44.929687-.171875-60.609375-.886719-15.644531-.714843-26.332031-3.199219-35.679687-6.832031-9.8125-3.691406-18.695313-9.476562-26.039063-16.957031-7.476562-7.339844-13.261719-16.226563-16.953125-26.035157-3.632812-9.347656-6.121094-20.035156-6.832031-35.679687-.722656-15.679687-.890625-20.6875-.890625-60.609375s.167969-44.929688.886719-60.605469c.710937-15.648437 3.195312-26.332031 6.828125-35.683593 3.691406-9.808594 9.480468-18.695313 16.960937-26.035157 7.339844-7.480469 16.226563-13.265625 26.035157-16.957031 9.351562-3.632812 20.035156-6.117188 35.683593-6.832031 15.675781-.714844 20.683594-.886719 60.605469-.886719s44.929688.171875 60.605469.890625c15.648437.710937 26.332031 3.195313 35.683593 6.824219 9.808594 3.691406 18.695313 9.480468 26.039063 16.960937 7.476563 7.34375 13.265625 16.226563 16.953125 26.035157 3.636719 9.351562 6.121094 20.035156 6.835938 35.683593.714843 15.675781.882812 20.683594.882812 60.605469s-.167969 44.929688-.886719 60.605469zm0 0"/></svg>
                        </a>

                        <a href="https://www.instagram.com/cleanclic/" target="_BLANK">
                            <svg height="30px" viewBox="0 0 512 512" width="30px" style="fill: #fff;" xmlns="http://www.w3.org/2000/svg"><path d="m224.113281 303.960938 83.273438-47.960938-83.273438-47.960938zm0 0"/><path d="m256 0c-141.363281 0-256 114.636719-256 256s114.636719 256 256 256 256-114.636719 256-256-114.636719-256-256-256zm159.960938 256.261719s0 51.917969-6.585938 76.953125c-3.691406 13.703125-14.496094 24.507812-28.199219 28.195312-25.035156 6.589844-125.175781 6.589844-125.175781 6.589844s-99.878906 0-125.175781-6.851562c-13.703125-3.6875-24.507813-14.496094-28.199219-28.199219-6.589844-24.769531-6.589844-76.949219-6.589844-76.949219s0-51.914062 6.589844-76.949219c3.6875-13.703125 14.757812-24.773437 28.199219-28.460937 25.035156-6.589844 125.175781-6.589844 125.175781-6.589844s100.140625 0 125.175781 6.851562c13.703125 3.6875 24.507813 14.496094 28.199219 28.199219 6.851562 25.035157 6.585938 77.210938 6.585938 77.210938zm0 0"/></svg>
                        </a>

                        <a href="https://www.youtube.com/channel/UCBJCTzKLWGiPjxJYDlzEznQ" target="_BLANK">
                            <svg height="30px" viewBox="0 0 512 512" width="30px"  style="fill: #fff;"xmlns="http://www.w3.org/2000/svg"><path d="m256 0c-141.363281 0-256 114.636719-256 256s114.636719 256 256 256 256-114.636719 256-256-114.636719-256-256-256zm5.425781 405.050781c-.003906 0 .003907 0 0 0h-.0625c-25.644531-.011719-50.84375-6.441406-73.222656-18.644531l-81.222656 21.300781 21.738281-79.375c-13.410156-23.226562-20.464844-49.578125-20.453125-76.574219.035156-84.453124 68.769531-153.160156 153.222656-153.160156 40.984375.015625 79.457031 15.96875 108.382813 44.917969 28.929687 28.953125 44.851562 67.4375 44.835937 108.363281-.035156 84.457032-68.777343 153.171875-153.21875 153.171875zm0 0"/><path d="m261.476562 124.46875c-70.246093 0-127.375 57.105469-127.40625 127.300781-.007812 24.054688 6.726563 47.480469 19.472657 67.75l3.027343 4.816407-12.867187 46.980468 48.199219-12.640625 4.652344 2.757813c19.550781 11.601562 41.964843 17.738281 64.816406 17.746094h.050781c70.191406 0 127.320313-57.109376 127.351563-127.308594.011718-34.019532-13.222657-66.003906-37.265626-90.066406-24.042968-24.0625-56.019531-37.324219-90.03125-37.335938zm74.90625 182.035156c-3.191406 8.9375-18.484374 17.097656-25.839843 18.199219-6.597657.984375-14.941407 1.394531-24.113281-1.515625-5.5625-1.765625-12.691407-4.121094-21.828126-8.0625-38.402343-16.578125-63.484374-55.234375-65.398437-57.789062-1.914063-2.554688-15.632813-20.753907-15.632813-39.59375 0-18.835938 9.890626-28.097657 13.398438-31.925782 3.511719-3.832031 7.660156-4.789062 10.210938-4.789062 2.550781 0 5.105468.023437 7.335937.132812 2.351563.117188 5.507813-.894531 8.613281 6.570313 3.191406 7.664062 10.847656 26.5 11.804688 28.414062.957031 1.917969 1.59375 4.152344.320312 6.707031-1.277344 2.554688-5.519531 8.066407-9.570312 13.089844-1.699219 2.105469-3.914063 3.980469-1.679688 7.8125 2.230469 3.828125 9.917969 16.363282 21.296875 26.511719 14.625 13.039063 26.960938 17.078125 30.789063 18.996094 3.824218 1.914062 6.058594 1.59375 8.292968-.957031 2.230469-2.554688 9.570313-11.175782 12.121094-15.007813 2.550782-3.832031 5.105469-3.191406 8.613282-1.914063 3.511718 1.273438 22.332031 10.535157 26.160156 12.449219 3.828125 1.917969 6.378906 2.875 7.335937 4.472657.960938 1.597656.960938 9.257812-2.230469 18.199218zm0 0"/></svg>
                        </a>

                        <a href="#." class="social"> Redes sociais</a>
                    </div>

                    <div class="col-lg-6 align-self-center copy">
                        Desenvolvido por: <a class="font-weight-bold kf-blue kf-links" href="https://www.creativedd.com.br/" target="_blank">Creative Dev & Design</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <?php include("includes/top.php")?>
    <?php include("includes/modal.php")?>
    <?php include("includes/scripts.php")?>
    
  </body>
</html>