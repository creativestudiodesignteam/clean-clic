<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <?php include("includes/head.php")?>
    <link rel="stylesheet" href="assets/css/wizard.css" />
  </head>
  <body class="register-body">
    <?php include("includes/load.php")?>
        
    <header class="header-register">
    <?php include("includes/menu-page.php")?>

        <div class="main d-flex align-items-center">
            
        <div class="container-fluid">
             <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <form method="POST" id="signup-form" class="signup-form">
                            <h3>
                                <span class="icon"><i class="ti-user"></i></span>
                                <span class="title_text">Personal</span>
                            </h3>
                            <fieldset>
                                <legend>
                                    <p class="step-number-title">Passo 1 de 4</p>
                                    <span class="step-heading">Cadastre-se agora e tenha<br><span class="bold-title">uma energia limpa e barata</span></span>
                                    <span class="step-number">Dados de contato <span class="option">01</span></span>
                                </legend>

                                <div class="form-row">
                                    <div class="form-group input-material col-lg-6">
                                        <input type="text" class="form-control" name="first_name" id="first_name" placeholder="Nome / Razão Social"/>
                                    </div>
            
                                    <div class="form-group input-material col-lg-6">
                                        <input type="text" class="form-control" name="first_name" id="first_name" placeholder="Sobrenome / Nome fantasia"/>
                                    </div>
            
                                    <div class="form-group input-material col-lg-6">
                                        <input type="text" class="form-control" name="first_name" id="first_name" placeholder="Digite seu e-mail"/>
                                    </div>
            
                                    <div class="form-group input-material col-lg-6">
                                        <input type="text" class="form-control" name="first_name" id="first_name" placeholder="Digite seu telefone"/>
                                    </div>
                                    
                                    <div class="form-group input-material col-lg-6">
                                        <input type="text" class="form-control" name="first_name" id="first_name" placeholder="CPF / CNPJ"/>
                                    </div>

                                    <div class="form-group input-material col-lg-6">
                                        <input type="text" class="form-control" name="first_name" id="first_name" placeholder="Informe sua cooperativa"/>
                                    </div>
                                </div>
                            </fieldset>
        
                            <h3>
                                <span class="icon"><i class="ti-email"></i></span>
                                <span class="title_text">Contact</span>
                            </h3>
                            <fieldset>
                                <legend>
                                    <p class="step-number-title">Passo 2 de 4</p>
                                    <span class="step-heading">Cadastre-se agora e tenha<br><span class="bold-title">uma energia limpa e barata</span></span>
                                    <span class="step-number">Endereço <span class="option">02</span></span>
                                </legend>
                                <div class="form-row">
                                    <div class="form-group input-material col-lg-6">
                                        <input type="text" class="form-control" name="first_name" id="first_name" placeholder="CEP"/>
                                    </div>
            
                                    <div class="form-group input-material col-lg-6">
                                        <input type="text" class="form-control" name="first_name" id="first_name" placeholder="Bairro"/>
                                    </div>
            
                                    <div class="form-group input-material col-lg-6">
                                        <input type="text" class="form-control" name="first_name" id="first_name" placeholder="Cidade"/>
                                    </div>
            
                                    <div class="form-group input-material col-lg-6">
                                        <input type="text" class="form-control" name="first_name" id="first_name" placeholder="UF"/>
                                    </div>
                                    
                                    <div class="form-group input-material col-lg-6">
                                        <input type="text" class="form-control" name="first_name" id="first_name" placeholder="Endereço"/>
                                    </div>

                                    <div class="form-group input-material col-lg-3">
                                        <input type="text" class="form-control" name="first_name" id="first_name" placeholder="Número"/>
                                    </div>

                                    <div class="form-group input-material col-lg-3">
                                        <input type="text" class="form-control" name="first_name" id="first_name" placeholder="Complemento"/>
                                    </div>
                                </div>
        
                                <!-- <div class="form-select">
                                    <label for="country" class="form-label">Country</label>
                                    <div class="select-list">
                                        <select name="country" id="country">
                                            <option value="">Australia</option>
                                            <option value="Australia">Australia</option>
                                            <option value="USA">America</option>
                                        </select>
                                    </div>
                                </div> -->
                            </fieldset>
        
                            <h3>
                                <span class="icon"><i class="ti-star"></i></span>
                                <span class="title_text">Offical</span>
                            </h3>
                            <fieldset>
                                <legend>
                                    <p class="step-number-title">Passo 3 de 4</p>
                                    <span class="step-heading">Cadastre-se agora e tenha<br><span class="bold-title">uma energia limpa e barata</span></span>
                                    <span class="step-number">Conta de luz <span class="option">03</span></span>
                                </legend>
                                <div class="form-row">
                                    <div class="form-group input-material col-lg-12">
                                        <input type="text" class="form-control" name="first_name" id="first_name" placeholder="Número da instalação"/>
                                    </div>
            
                                    <div class="form-group col-lg-12">
                                        <input type="file" class="custom-file-input form-control" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01">
                                        <label class="custom-file-label" for="inputGroupFile01">Enviar foto</label>
                                        
                                    </div>
                                </div>
                            </fieldset>
        
                            <h3>
                                <span class="icon"><i class="ti-credit-card"></i></span>
                                <span class="title_text">Payment</span>
                            </h3>
                            <fieldset>
                                <legend>
                                    <p class="step-number-title">Passo 4 de 4</p>
                                    <span class="step-heading">Cadastre-se agora e tenha<br><span class="bold-title">uma energia limpa e barata</span></span>
                                    <span class="step-number">Acessos <span class="option">04</span></span>
                                </legend>
                                <div class="form-row">
                                    <div class="form-group input-material col-lg-6">
                                        <input type="text" class="form-control" name="first_name" id="first_name" placeholder="E-mail"/>
                                    </div>
            
                                    <div class="form-group input-material col-lg-6">
                                        <input type="text" class="form-control" name="first_name" id="first_name" placeholder="Confirmar e-mail"/>
                                    </div>
            
                                    <div class="form-group input-material col-lg-6">
                                        <input type="password" class="form-control" name="first_name" id="first_name" placeholder="Senha"/>
                                    </div>
            
                                    <div class="form-group input-material col-lg-6">
                                        <input type="password" class="form-control" name="first_name" id="first_name" placeholder="Confirmar senha"/>
                                    </div>
                                </div>
                            </fieldset>
                    </form>
                </div>
            </div>
        </div>

    </div>
    </header>
    <?php include("includes/scripts.php")?>
    <script src="vendor/jquery-validation/dist/jquery.validate.min.js"></script>
    <script src="vendor/jquery-validation/dist/additional-methods.min.js"></script>
    <script src="vendor/jquery-steps/jquery.steps.min.js"></script>
    <script src="vendor/minimalist-picker/dobpicker.js"></script>
    <script>
        (function($) {
            var form = $("#signup-form");
            form.validate({
                errorPlacement: function errorPlacement(error, element) {
                    element.before(error); 
                },
                rules: {
                    first_name : {
                        required: false,
                    },
                    last_name : {
                        required: false,
                    },
                    user_name : {
                        required: false,
                    },
                    password : {
                        required: false,
                    },
                    email : {
                        required: false,
                    },
                    phone : {
                        required: false,
                    },
                    address: {
                        required: false,
                    },
                    employee_id : {
                        required: false,
                    },
                    designation: {
                        required: false,
                    },
                    department: {
                        required: false,
                    },
                    work_hours: {
                        required: false,
                    },
                    bank_name: {
                        required: false,
                    },
                    holder_name: {
                        required: false,
                    },
                    card_number: {
                        required: false,
                        number: false,
                    },
                    cvc: {
                        required: false,
                    },
                },
                onfocusout: function(element) {
                    $(element).valid();
                },
                highlight : function(element, errorClass, validClass) {
                    $(element.form).find('.actions').addClass('form-error');
                    $(element).removeClass('valid');
                    $(element).addClass('error');
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element.form).find('.actions').removeClass('form-error');
                    $(element).removeClass('error');
                    $(element).addClass('valid');
                }
            });
            form.steps({
                headerTag: "h3",
                bodyTag: "fieldset",
                transitionEffect: "fade",
                labels: {
                    previous : 'voltar',
                    next : 'Continuar',
                    finish : 'Cadastrar',
                    current : ''
                },
                titleTemplate : '<span class="title">#title#</span>',
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    form.validate().settings.ignore = ":disabled,:hidden";
                    return form.valid();
                },
                onFinishing: function (event, currentIndex)
                {
                    form.validate().settings.ignore = ":disabled";
                    return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    alert('Sumited');
                },
                // onInit : function (event, currentIndex) {
                //     event.append('demo');
                // }
            });

            jQuery.extend(jQuery.validator.messages, {
                required: "",
                remote: "",
                email: "",
                url: "",
                date: "",
                dateISO: "",
                number: "",
                digits: "",
                creditcard: "",
                equalTo: ""
            });

            $('#gender').parent().append('<ul class="list-item" id="newgender" name="gender"></ul>');
            $('#gender option').each(function(){
                $('#newgender').append('<li value="' + $(this).val() + '">'+$(this).text()+'</li>');
            });
            $('#gender').remove();
            $('#newgender').attr('id', 'gender');
            $('#gender li').first().addClass('init');
            $("#gender").on("click", ".init", function() {
                $(this).closest("#gender").children('li:not(.init)').toggle();
            });

            var allOptions = $("#gender").children('li:not(.init)');
            $("#gender").on("click", "li:not(.init)", function() {
                allOptions.removeClass('selected');
                $(this).addClass('selected');
                $("#gender").children('.init').html($(this).html());
                allOptions.toggle();
            });

            $('#country').parent().append('<ul class="list-item" id="newcountry" name="country"></ul>');
            $('#country option').each(function(){
                $('#newcountry').append('<li value="' + $(this).val() + '">'+$(this).text()+'</li>');
            });
            $('#country').remove();
            $('#newcountry').attr('id', 'country');
            $('#country li').first().addClass('init');
            $("#country").on("click", ".init", function() {
                $(this).closest("#country").children('li:not(.init)').toggle();
            });

            var CountryOptions = $("#country").children('li:not(.init)');
            $("#country").on("click", "li:not(.init)", function() {
                CountryOptions.removeClass('selected');
                $(this).addClass('selected');
                $("#country").children('.init').html($(this).html());
                CountryOptions.toggle();
            });

            $('#payment_type').parent().append('<ul  class="list-item" id="newpayment_type" name="payment_type"></ul>');
            $('#payment_type option').each(function(){
                $('#newpayment_type').append('<li value="' + $(this).val() + '">'+$(this).text()+'</li>');
            });
            $('#payment_type').remove();
            $('#newpayment_type').attr('id', 'payment_type');
            $('#payment_type li').first().addClass('init');
            $("#payment_type").on("click", ".init", function() {
                $(this).closest("#payment_type").children('li:not(.init)').toggle();
            });

            var PaymentsOptions = $("#payment_type").children('li:not(.init)');
            $("#payment_type").on("click", "li:not(.init)", function() {
                PaymentsOptions.removeClass('selected');
                $(this).addClass('selected');
                $("#payment_type").children('.init').html($(this).html());
                PaymentsOptions.toggle();
            });

            $.dobPicker({
                daySelector: '#birth_date',
                monthSelector: '#birth_month',
                yearSelector: '#birth_year',
                dayDefault: 'Day',
                monthDefault: 'Month',
                yearDefault: 'Year',
                minimumAge: 0,
                maximumAge: 120
            });

            $.dobPicker({
                daySelector: '#expiry_date',
                monthSelector: '#expiry_month',
                yearSelector: '#expiry_year',
                dayDefault: 'Day',
                monthDefault: 'Month',
                yearDefault: 'Year',
                minimumAge: 0,
                maximumAge: 120
            });
                
            })(jQuery);

    </script>
    
  </body>
</html>