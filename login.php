<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <?php include("includes/head.php")?>
  </head>
  <body>
    <?php include("includes/load.php")?>
        
    <header class="header-login">
    <?php include("includes/menu-page.php")?>

        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-7 login align-self-center">
                    
                </div>

                <div class="col-10 col-lg-3 offset-1 align-self-center">
                    <h1 class="title-form">Acesso<br><span class="bold-title">ao sistema</span></h1>
                    <form id="contact-form">
                        <div class="form-row">
                            <div class="form-group input-material col-lg-12">
                                <input type="email" class="form-control" id="name-field" required>
                                <label for="name-field">Digite seu e-mail</label>
                            </div>
                            <div class="form-group input-material col-lg-12">
                                <input type="password" class="form-control" id="email-field" required>
                                <label for="email-field">Informe sua senha</label>
                            </div>

                            <div class="form-group col-lg-12 send mt-4">
                                <a href="#." class="cta-dark mt-3 button" data-dismiss="modal">
                                    <span>Acessar</span>
                                    <svg width="13px" height="10px" viewBox="0 0 13 10">
                                        <path d="M1,5 L11,5"></path>
                                        <polyline points="8 1 12 5 8 9"></polyline>
                                    </svg>
                                </a>

                                <a href="esqueci-senha.php" class="forgot">Esqueceu a senha?</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </header>
    <?php include("includes/scripts.php")?>
    
  </body>
</html>