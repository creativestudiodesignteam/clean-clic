<nav class="navbar navbar-expand-lg navbar-dark bg-transparent bg-mobile-color">
        <div class="container-fluid">
            <a class="navbar-brand" href="#home">
                <img src="assets/images/logo.png" class="logo" alt="CleanClic" title="CleanClic" />
                <img src="assets/images/logo-c.png" class="logo-c d-none" alt="CleanClic" title="CleanClic" />
            </a>
            <div class="navbar-collapse" id="conteudoNavbarSuportado">
                <section class="section section--menu navbar-nav m-auto d-none d-lg-block" id="Prospero">
                    <nav class="menu menu--prospero">
                        <ul class="menu__list">
                            <li class="nav-item menu__item menu__item--current">
                                <a href="#home" class="menu__link">Home</a>
                            </li>
                            <li class="nav-item menu__item">
                                <a href="#sobre" class="menu__link">Sobre nós</a>
                            </li>
                            <li class="nav-item menu__item">
                                <a href="#parceiros" class="menu__link">Parceiros</a>
                            </li>
                            <li class="nav-item menu__item">
                                <a href="#simulador" class="menu__link">Seja um clic</a>
                            </li>
                            <li class="nav-item menu__item">
                                <a href="#faq" class="menu__link">FAQ</a>
                            </li>
                            <li class="nav-item menu__item">
                                <a href="#contato" class="menu__link">Contato</a>
                            </li>
                        </ul>
                    </nav>
                </section>

                <div class="my-2 my-lg-0">
                    <ul class="navbar-nav">
                        <li class="nav-item action-menu">
                            <a class="nav-link" href="login.php">Entrar</a>
                        </li>
                        <li class="nav-item action-menu">
                            <a class="nav-link" href="cadastro.php">Cadastro
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                    viewBox="0 0 31.49 31.49" style="enable-background:new 0 0 31.49 31.49;" width="14px" xml:space="preserve">
                                    <path style="fill:#fff;" d="M21.205,5.007c-0.429-0.444-1.143-0.444-1.587,0c-0.429,0.429-0.429,1.143,0,1.571l8.047,8.047H1.111
                                        C0.492,14.626,0,15.118,0,15.737c0,0.619,0.492,1.127,1.111,1.127h26.554l-8.047,8.032c-0.429,0.444-0.429,1.159,0,1.587
                                        c0.444,0.444,1.159,0.444,1.587,0l9.952-9.952c0.444-0.429,0.444-1.143,0-1.571L21.205,5.007z"/>
                                </svg>
                            </a>
                        </li>
 
                        <li class="nav-item action-menu">
                            <a class="nav-link lang active" href="#.">Pt</a>
                        </li>

                        <li class="nav-item action-menu">
                            <a class="nav-link lang mr-3" href="#.">En</a>
                        </li>

                        <li class="nav-item toggle" id="toggle-meu-full">
                            <button class="nav-link menu-mobile menu-icon">
                                <div class="bar-menu"></div>
                                <div class="bar-menu"></div>
                                <div class="bar-menu"></div>
                            </button>
                        </li>

                        <li class="nav-item d-none d-md-block">
                            <a href="#contato" class="contact-fixed-btn action-btn">
                                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                    viewBox="0 0 480.56 480.56" style="enable-background:new 0 0 480.56 480.56;" width="18px" xml:space="preserve">
                                    <g>
                                        <g>
                                            <path d="M365.354,317.9c-15.7-15.5-35.3-15.5-50.9,0c-11.9,11.8-23.8,23.6-35.5,35.6c-3.2,3.3-5.9,4-9.8,1.8
                                                c-7.7-4.2-15.9-7.6-23.3-12.2c-34.5-21.7-63.4-49.6-89-81c-12.7-15.6-24-32.3-31.9-51.1c-1.6-3.8-1.3-6.3,1.8-9.4
                                                c11.9-11.5,23.5-23.3,35.2-35.1c16.3-16.4,16.3-35.6-0.1-52.1c-9.3-9.4-18.6-18.6-27.9-28c-9.6-9.6-19.1-19.3-28.8-28.8
                                                c-15.7-15.3-35.3-15.3-50.9,0.1c-12,11.8-23.5,23.9-35.7,35.5c-11.3,10.7-17,23.8-18.2,39.1c-1.9,24.9,4.2,48.4,12.8,71.3
                                                c17.6,47.4,44.4,89.5,76.9,128.1c43.9,52.2,96.3,93.5,157.6,123.3c27.6,13.4,56.2,23.7,87.3,25.4c21.4,1.2,40-4.2,54.9-20.9
                                                c10.2-11.4,21.7-21.8,32.5-32.7c16-16.2,16.1-35.8,0.2-51.8C403.554,355.9,384.454,336.9,365.354,317.9z"/>
                                            <path d="M346.254,238.2l36.9-6.3c-5.8-33.9-21.8-64.6-46.1-89c-25.7-25.7-58.2-41.9-94-46.9l-5.2,37.1
                                                c27.7,3.9,52.9,16.4,72.8,36.3C329.454,188.2,341.754,212,346.254,238.2z"/>
                                            <path d="M403.954,77.8c-42.6-42.6-96.5-69.5-156-77.8l-5.2,37.1c51.4,7.2,98,30.5,134.8,67.2c34.9,34.9,57.8,79,66.1,127.5
                                                l36.9-6.3C470.854,169.3,444.354,118.3,403.954,77.8z"/>
                                        </g>
                                    </g>
                                </svg>
                            </a>
                        </li>
                        
                        <li class="nav-item d-none d-md-block">
                            <div class="dropdown lang-fixed-btn action-btn">
                                <button id="dLabel" class="dropdown-select" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    PT
                                    <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
                                    <svg class="caret" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                        width="5px" height="5px" viewBox="0 0 292.362 292.362" style="enable-background:new 0 0 292.362 292.362; fill: #5C5C5C;"
                                        xml:space="preserve">
                                        <g>
                                            <path d="M286.935,69.377c-3.614-3.617-7.898-5.424-12.848-5.424H18.274c-4.952,0-9.233,1.807-12.85,5.424
                                                C1.807,72.998,0,77.279,0,82.228c0,4.948,1.807,9.229,5.424,12.847l127.907,127.907c3.621,3.617,7.902,5.428,12.85,5.428
                                                s9.233-1.811,12.847-5.428L286.935,95.074c3.613-3.617,5.427-7.898,5.427-12.847C292.362,77.279,290.548,72.998,286.935,69.377z"/>
                                        </g>
                                    </svg>  
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dLabel">
                                    <li>PT</li>
                                    <li>EN</li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>