<!-- Meta tags Obrigatórias -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="assets/css/main.css" />
<link rel="stylesheet" href="assets/css/input-material.css" />
<link rel="stylesheet" href="node_modules/animate.css/animate.min.css" />
<link rel="stylesheet" href="node_modules/wow.js/css/libs/animate.css" />
<link rel="stylesheet" href="node_modules/owl.carousel2/dist/assets/owl.carousel.min.css" />
<link rel="stylesheet" href="assets/font/flaticon.css" />

<link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon" />

<title>ClenClic</title>

<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">