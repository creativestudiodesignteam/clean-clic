<main class="main-content" id="home">
        <section class="slideshow">
            <div class="slideshow-inner">
                <div class="slides">
                    <div class="slide is-active ">
                        <div class="slide-content">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-lg-5 offset-lg-1 align-self-center">
                                        <div class="caption">
                                            <span class="number">01</span>
                                            <div class="title mb-4">Energia limpa<br><span>e de melhor custo para você!</span></div>
                                            <!-- <div class="text mb-4">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                            </div>  -->
                                            <a href="#" class="cta">
                                                <span>Quero ser um clic</span>
                                                <svg width="13px" height="10px" viewBox="0 0 13 10">
                                                    <path d="M1,5 L11,5"></path>
                                                    <polyline points="8 1 12 5 8 9"></polyline>
                                                </svg>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 p-0 d-none d-lg-block">
                                        <img src="assets/images/casal.png" class="img-fluid image-slide" alt="Família Feliz" />
                                    </div>
                                </div>
                            </div>

                            <div class="item-hints d-none d-lg-block">
                                <div class="hint -one-hint" data-position="4">
                                    <span class="hint-radius"></span>
                                    <span class="hint-dot"></span>
                                    <div class="hint-content do--split-children">
                                    <p><strong>Geração distribuída</strong> é o termo utilizado para referenciar a energia elétrica que é gerada próxima ou no local de consumo.</p>
                                    </div>
                                </div>

                                <div class="hint -two-hint" data-position="1">
                                    <span class="hint-radius"></span>
                                    <span class="hint-dot"></span>
                                    <div class="hint-content do--split-children">
                                    <p>Por possuir muitas vantagens, a cada ano cresce o número de consumidores que ingressam na <strong>Geração Compartilhada</strong> de energia no Brasil.</p>
                                    </div>
                                </div>

                                <div class="hint -three-hint" data-position="4">
                                    <span class="hint-radius"></span>
                                    <span class="hint-dot"></span>
                                    <div class="hint-content do--split-children">
                                    <p>A modalidade Geração Compartilhada é bastante diferente da geração centralizada, em que grandes usinas geradoras produzem energia e enviam às concessionárias locais que distribuem aos consumidores através de linhas de transmissão.</p>
                                    </div>
                                </div>

                                <div class="hint -four-hint" data-position="3">
                                    <span class="hint-radius"></span>
                                    <span class="hint-dot"></span>
                                    <div class="hint-content do--split-children">
                                    <p>No modelo de Geração Compartilhada, os sistemas geradores ficam próximos ou até mesmo na própria unidade consumidora (casas, empresas e indústrias).</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="image-container"> 
                            <img src="assets/images/bg-header.jpg" alt="Energia de melhor custo" class="image" />
                        </div>
                    </div>

                    <div class="slide">
                        <div class="slide-content">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-lg-5 offset-lg-1 align-self-center">
                                        <div class="caption">
                                            <span class="number">02</span>
                                            <div class="title mb-4">Na Geração Distribuída <br><span>você compartilha sua energia com o mundo</span></div>
                                            <!-- <div class="text mb-4">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                            </div> --> 
                                            <a href="#" class="cta">
                                                <span>Quero ser um clic</span>
                                                <svg width="13px" height="10px" viewBox="0 0 13 10">
                                                    <path d="M1,5 L11,5"></path>
                                                    <polyline points="8 1 12 5 8 9"></polyline>
                                                </svg>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 p-0 d-none d-md-block">
                                        <img src="assets/images/hand.png" class="img-fluid image-slide" alt="Família Feliz" />
                                    </div>
                                </div>
                            </div>

                            <!-- <div class="item-hints d-none d-md-block">
                                <div class="hint -one-hint" data-position="4">
                                    <span class="hint-radius"></span>
                                    <span class="hint-dot"></span>
                                    <div class="hint-content do--split-children">
                                    <p>The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.</p>
                                    </div>
                                </div>

                                <div class="hint -two-hint" data-position="1">
                                    <span class="hint-radius"></span>
                                    <span class="hint-dot"></span>
                                    <div class="hint-content do--split-children">
                                    <p>The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.</p>
                                    </div>
                                </div>

                                <div class="hint -three-hint" data-position="4">
                                    <span class="hint-radius"></span>
                                    <span class="hint-dot"></span>
                                    <div class="hint-content do--split-children">
                                    <p>The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.</p>
                                    </div>
                                </div>

                                <div class="hint -four-hint" data-position="3">
                                    <span class="hint-radius"></span>
                                    <span class="hint-dot"></span>
                                    <div class="hint-content do--split-children">
                                    <p>The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.</p>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                        <div class="image-container">
                            <img src="assets/images/bg-header.jpg" alt="Energia limpa e renovável" class="image" />
                        </div>
                    </div>
                </div>

                <div class="arrows">
                    <div class="arrow prev">
                        <button class="spin circle">
                            <span class="svg svg-arrow-left">
                                <svg version="1.1" id="svg4-Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="14px" height="26px" viewBox="0 0 14 26" enable-background="new 0 0 14 26" xml:space="preserve"> <path d="M13,26c-0.256,0-0.512-0.098-0.707-0.293l-12-12c-0.391-0.391-0.391-1.023,0-1.414l12-12c0.391-0.391,1.023-0.391,1.414,0s0.391,1.023,0,1.414L2.414,13l11.293,11.293c0.391,0.391,0.391,1.023,0,1.414C13.512,25.902,13.256,26,13,26z"/> </svg>
                                <span class="alt sr-only"></span>
                            </span>
                        </button>
                    </div>

                    <div class="arrow next">
                        <button class="spin circle">
                            <span class="svg svg-arrow-right">
                                <svg version="1.1" id="svg5-Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="14px" height="26px" viewBox="0 0 14 26" enable-background="new 0 0 14 26" xml:space="preserve"> <path d="M1,0c0.256,0,0.512,0.098,0.707,0.293l12,12c0.391,0.391,0.391,1.023,0,1.414l-12,12c-0.391,0.391-1.023,0.391-1.414,0s-0.391-1.023,0-1.414L11.586,13L0.293,1.707c-0.391-0.391-0.391-1.023,0-1.414C0.488,0.098,0.744,0,1,0z"/> </svg>
                                <span class="alt sr-only"></span>
                            </span>
                        </button>

                    </div>
                </div>
            </div> 
        </section>
    </main>