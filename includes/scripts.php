<!-- JavaScript (Opcional) -->
    <!-- jQuery primeiro, depois Popper.js, depois Bootstrap JS -->
    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.1/TweenMax.min.js"></script>
    <script src="node_modules/popper.js/dist/popper.min.js"></script>
    <script src="node_modules/owl.carousel2/dist/owl.carousel.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="node_modules/jquery.nicescroll/dist/jquery.nicescroll.min.js"></script>
    <script src="node_modules/wow.js/dist/wow.min.js"></script>
    <script src="node_modules/notifyjs/dist/notify.js"></script>
    <script src="assets/js/classie.js"></script>
    <script src="assets/js/materialize-inputs.jquery.js"></script>
    
    <script src="assets/js/app.js"></script>
        <script>
            var wow = new WOW(
            {
                boxClass:     'wow',      // animated element css class (default is wow)
                animateClass: 'animated', // animation css class (default is animated)
                offset:       0,          // distance to the element when triggering the animation (default is 0)
                mobile:       true,       // trigger animations on mobile devices (default is true)
                live:         true,       // act on asynchronously loaded content (default is true)
                callback:     function(box) {
                // the callback is fired every time an animation is started
                // the argument that is passed in is the DOM node being animated
                },
                scrollContainer: null,    // optional scroll container selector, otherwise use window,
                resetAnimation: true,     // reset animation on end (default is true)
            }
            );
            wow.init();
        </script>

        <script>
            $('document').ready(function(){
                // enable material-style inputs in entire body
                $('body').materializeInputs();

            });
        </script>

        <script>
            (function() {
                [].slice.call(document.querySelectorAll('.menu')).forEach(function(menu) {
                    var menuItems = menu.querySelectorAll('.menu__link'),
                        setCurrent = function(ev) {
                            ev.preventDefault();

                            var item = ev.target.parentNode; // li

                            // return if already current
                            if (classie.has(item, 'menu__item--current')) {
                                return false;
                            }
                            // remove current
                            classie.remove(menu.querySelector('.menu__item--current'), 'menu__item--current');
                            // set current
                            classie.add(item, 'menu__item--current');
                        };

                    [].slice.call(menuItems).forEach(function(el) {
                        el.addEventListener('click', setCurrent);
                    });
                });

                [].slice.call(document.querySelectorAll('.link-copy')).forEach(function(link) {
                    link.setAttribute('data-clipboard-text', location.protocol + '//' + location.host + location.pathname + '#' + link.parentNode.id);
                    new Clipboard(link);
                    link.addEventListener('click', function() {
                        classie.add(link, 'link-copy--animate');
                        setTimeout(function() {
                            classie.remove(link, 'link-copy--animate');
                        }, 300);
                    });
                });
            })(window);
		</script>