$(function () {$("#preloader").fadeOut();});

$( "#send-contact" ).click(function() {
  $.notify("Hello World");
});

/*Menu dek and mobile*/
$('.dropdown-menu li').on('click', function() {
  var getValue = $(this).text();
  $('.dropdown-select').text(getValue);
});


/* $('.dropdown-menu2 li').on('click', function() {
  var getValue = $(this).text();
  $('.dropdown-select').text(getValue);
}); */

$(window).scroll(function() {    
    var scroll = $(window).scrollTop();

    if (scroll > 0) {
        $(".navbar").addClass("fixed-menu");
        $(".action-btn").removeClass("d-none");
        $(".logo").addClass("d-none");
        $(".logo-c").removeClass("d-none");
    } else{
        $(".navbar").removeClass("fixed-menu");
        $(".action-btn").addClass("d-none");
        $(".logo").removeClass("d-none");
        $(".logo-c").addClass("d-none");
        
    }


    if (scroll >= 250) {
        $('.menu--prospero').fadeOut();
        $('.action-menu').fadeOut();
        $('.action-btn').fadeIn();
        $(".action-btn").addClass("btn-contact-toggle");
        $(".lang-fixed-btn").addClass("btn-lang-toggle");
        $(".lang-fixed-btn").addClass("btn-lang-toggle");
        
      } else {
        $('.menu--prospero').fadeIn();
        $('.action-menu').fadeIn();
        $(".action-btn").removeClass("btn-contact-toggle");
        $(".lang-fixed-btn").removeClass("btn-lang-toggle");
        
    }
});


btnClose = false;
$('.toggle, .close, .menu-link').click(function() {
    $(this).toggleClass('active');
    $('#overlay').toggleClass('open');
    if (btnClose == false) {
      $('.close').css("display", "block" );
      $('.close').addClass('active' );
      $('body').addClass('overflow-hidden' );
      btnClose = true;
    }else{
      $('.close').css("display", "none" );
      $('.close').removeClass('active' );
      $('body').removeClass('overflow-hidden');
      btnClose = false;
    }
 });




/*Slide header*/
  var slideshowDuration = 500000;
  var slideshow=$('.main-content .slideshow');

  function slideshowSwitch(slideshow,index,auto){
  if(slideshow.data('wait')) return;

  var slides = slideshow.find('.slide');
  var pages = slideshow.find('.pagination');
  var activeSlide = slides.filter('.is-active');
  var activeSlideImage = activeSlide.find('.image-container');
  var newSlide = slides.eq(index);
  var newSlideImage = newSlide.find('.image-container');
  var newSlideContent = newSlide.find('.slide-content');
  var newSlideElements=newSlide.find('.caption > *');
  if(newSlide.is(activeSlide))return;

  newSlide.addClass('is-new');
  var timeout=slideshow.data('timeout');
  clearTimeout(timeout);
  slideshow.data('wait',true);
  var transition=slideshow.attr('data-transition');
  if(transition=='fade'){
    newSlide.css({
      display:'block',
      zIndex:2
    });
    newSlideImage.css({
      opacity:0
    });

    TweenMax.to(newSlideImage,1,{
      alpha:1,
      onComplete:function(){
        newSlide.addClass('is-active').removeClass('is-new');
        activeSlide.removeClass('is-active');
        newSlide.css({display:'',zIndex:''});
        newSlideImage.css({opacity:''});
        slideshow.find('.pagination').trigger('check');
        slideshow.data('wait',false);
        if(auto){
          timeout=setTimeout(function(){
            slideshowNext(slideshow,false,true);
          },slideshowDuration);
          slideshow.data('timeout',timeout);}}});
  } else {
    if(newSlide.index()>activeSlide.index()){
      var newSlideRight=0;
      var newSlideLeft='auto';
      var newSlideImageRight=-slideshow.width()/8;
      var newSlideImageLeft='auto';
      var newSlideImageToRight=0;
      var newSlideImageToLeft='auto';
      var newSlideContentLeft='auto';
      var newSlideContentRight=0;
      var activeSlideImageLeft=-slideshow.width()/4;
    } else {
      var newSlideRight='';
      var newSlideLeft=0;
      var newSlideImageRight='auto';
      var newSlideImageLeft=-slideshow.width()/8;
      var newSlideImageToRight='';
      var newSlideImageToLeft=0;
      var newSlideContentLeft=0;
      var newSlideContentRight='auto';
      var activeSlideImageLeft=slideshow.width()/4;
    }

    newSlide.css({
      display:'block',
      width:0,
      right:newSlideRight,
      left:newSlideLeft
      ,zIndex:2
    });

    newSlideImage.css({
      width:slideshow.width(),
      right:newSlideImageRight,
      left:newSlideImageLeft
    });

    newSlideContent.css({
      width:slideshow.width(),
      left:newSlideContentLeft,
      right:newSlideContentRight
    });

    activeSlideImage.css({
      left:0
    });

    TweenMax.set(newSlideElements,{y:20,force3D:true});
    TweenMax.to(activeSlideImage,1,{
      left:activeSlideImageLeft,
      ease:Power3.easeInOut
    });

    TweenMax.to(newSlide,1,{
      width:slideshow.width(),
      ease:Power3.easeInOut
    });

    TweenMax.to(newSlideImage,1,{
      right:newSlideImageToRight,
      left:newSlideImageToLeft,
      ease:Power3.easeInOut
    });

    TweenMax.staggerFromTo(newSlideElements,0.8,{alpha:0,y:60},{alpha:1,y:0,ease:Power3.easeOut,force3D:true,delay:0.6},0.1,function(){
      newSlide.addClass('is-active').removeClass('is-new');
      activeSlide.removeClass('is-active');
      newSlide.css({
        display:'',
        width:'',
        left:'',
        zIndex:''
      });

      newSlideImage.css({
        width:'',
        right:'',
        left:''
      });

      newSlideContent.css({
        width:'',
        left:''
      });

      newSlideElements.css({
        opacity:'',
        transform:''
      });

      activeSlideImage.css({
        left:''
      });

      slideshow.find('.pagination').trigger('check');
      slideshow.data('wait',false);
      if(auto){
        timeout=setTimeout(function(){
          slideshowNext(slideshow,false,true);
        },slideshowDuration);
        slideshow.data('timeout',timeout);
      }
    });
  }
}

function slideshowNext(slideshow,previous,auto){
  var slides=slideshow.find('.slide');
  var activeSlide=slides.filter('.is-active');
  var newSlide=null;
  if(previous){
    newSlide=activeSlide.prev('.slide');
    if(newSlide.length === 0) {
      newSlide=slides.last();
    }
  } else {
    newSlide=activeSlide.next('.slide');
    if(newSlide.length==0)
      newSlide=slides.filter('.slide').first();
  }

  slideshowSwitch(slideshow,newSlide.index(),auto);
}

function homeSlideshowParallax(){
  var scrollTop=$(window).scrollTop();
  if(scrollTop>windowHeight) return;
  var inner=slideshow.find('.slideshow-inner');
  var newHeight=windowHeight-(scrollTop/2);
  var newTop=scrollTop*0.8;

  inner.css({
    transform:'translateY('+newTop+'px)',height:newHeight
  });
}

$(document).ready(function() {
 $('.slide').addClass('is-loaded');

 $('.slideshow .arrows .arrow').on('click',function(){
  slideshowNext($(this).closest('.slideshow'),$(this).hasClass('prev'));
});

 $('.slideshow .pagination .item').on('click',function(){
  slideshowSwitch($(this).closest('.slideshow'),$(this).index());
});

 $('.slideshow .pagination').on('check',function(){
  var slideshow=$(this).closest('.slideshow');
  var pages=$(this).find('.item');
  var index=slideshow.find('.slides .is-active').index();
  pages.removeClass('is-active');
  pages.eq(index).addClass('is-active');
});

/* Lazyloading
$('.slideshow').each(function(){
  var slideshow=$(this);
  var images=slideshow.find('.image').not('.is-loaded');
  images.on('loaded',function(){
    var image=$(this);
    var slide=image.closest('.slide');
    slide.addClass('is-loaded');
  });
*/

var timeout=setTimeout(function(){
  slideshowNext(slideshow,false,true);
},slideshowDuration);

slideshow.data('timeout',timeout);
});

if($('.main-content .slideshow').length > 1) {
  $(window).on('scroll',homeSlideshowParallax);
}

/*Modal Vídeo*/
$(document).ready(function() {
  $('#youtubeVideo').on('hidden.bs.modal', function() {
    var $this = $(this).find('iframe'),
      tempSrc = $this.attr('src');
    $this.attr('src', "");
    $this.attr('src', tempSrc);
  });
});

/*Select box custom*/
console.clear();

var el = {};

$('.placeholder').on('click', function (ev) {
  $('.placeholder').css('opacity', '0');
  $('.list__ul').toggle();
});

 $('.list__ul a').on('click', function (ev) {
   ev.preventDefault();
   var index = $(this).parent().index();
   
   $('.placeholder').text( $(this).text() ).css('opacity', '1');
   
   console.log($('.list__ul').find('li').eq(index).html());
   
   $('.list__ul').find('li').eq(index).prependTo('.list__ul');
   $('.list__ul').toggle();   
   
 });


$('select').on('change', function (e) {
  
  // Set text on placeholder hidden element
  $('.placeholder').text(this.value);
  
  // Animate select width as placeholder
  $(this).animate({width: $('.placeholder').width() + 'px' });
});

console.clear();

var el2 = {};

$('.placeholder-2').on('click', function (ev) {
  $('.placeholder-2').css('opacity', '0');
  $('.list__ul-2').toggle();
});

 $('.list__ul-2 a').on('click', function (ev) {
   ev.preventDefault();
   var index = $(this).parent().index();
   
   $('.placeholder-2').text( $(this).text() ).css('opacity', '1');
   
   console.log($('.list__ul-2').find('li').eq(index).html());
   
   $('.list__ul-2').find('li').eq(index).prependTo('.list__ul-2');
   $('.list__ul-2').toggle();   
   
 });


$('select').on('change', function (e) {
  
  // Set text on placeholder hidden element
  $('.placeholder-2').text(this.value);
  
  // Animate select width as placeholder
  $(this).animate({width: $('.placeholder-2').width() + 'px' });
});


/*Ranged slider*/

$(document).ready(function() {
  var val = $("#ranger").val();
  x = 170 + val/100 * (5000-170)
  var f = x.toLocaleString('pt-br',{style: 'currency', currency: 'BRL'});
  $("#valor-real").attr("data-slider-value",f);
});

let app = (() => {
  function updateSlider(element) {
    if (element) {
      let parent = element.parentElement,
      lastValue = parent.getAttribute('data-slider-value');

      if (lastValue === element.value) {
        return;
      }

      document.getElementById("ranger").addEventListener("change", function(evt) {
        x = 170 + this.value/100 * (5000-170)
        //var int = getMoney(x);
        var f = x.toLocaleString('pt-br',{style: 'currency', currency: 'BRL'});
        parent.setAttribute('data-slider-value', f);
        
        /* $(".mounth-valor").text(x*1); */
        var m = (x * 30)/50;
        $(".mounth-valor").text(m.toLocaleString('pt-br',{style: 'currency', currency: 'BRL'}));

        var pDesconto = parseFloat((x * 30)/100 );
        var conta = parseFloat(pDesconto.toFixed(2));   
        $(".economy-valor").text(pDesconto+"%");
      });

      //parent.setAttribute('data-slider-value', element.value);
      let $thumb = parent.querySelector('.range-slider__thumb'),
      $bar = parent.querySelector('.range-slider__bar'),
      pct = element.value * ((parent.clientHeight - $thumb.clientHeight) / parent.clientHeight);

      $thumb.style.bottom = `${pct}%`;
      $bar.style.height = `calc(${pct}% + ${$thumb.clientHeight/2}px)`;
      $thumb.innerHTML = "<svg version='1.1' id='Layer_1' width='18px' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px' viewBox='0 0 512 512' style='enable-background:new 0 0 512 512; fill: #18A074;' xml:space='preserve'><g><g><path d='M509.121,372.083l-246.17-246.175c-3.684-3.684-10.209-3.684-13.893,0L2.878,372.083c-3.838,3.838-3.838,10.055,0,13.893 c3.838,3.838,10.055,3.838,13.893,0l239.234-239.229l239.224,239.228c1.919,1.919,4.433,2.878,6.946,2.878 c2.514,0,5.028-0.959,6.946-2.878C512.959,382.139,512.959,375.921,509.121,372.083z'/></g></g></svg>";
    }
  }
  return {
    updateSlider: updateSlider
  };

})();

(function initAndSetupTheSliders() {
  const inputs = [].slice.call(document.querySelectorAll('.range-slider input'));
  inputs.forEach(input => input.setAttribute('value', '50'));
  inputs.forEach(input => app.updateSlider(input));
  // Cross-browser support where value changes instantly as you drag the handle, therefore two event types.
  inputs.forEach(input => input.addEventListener('input', element => app.updateSlider(input)));
  inputs.forEach(input => input.addEventListener('change', element => app.updateSlider(input)));
})();

/*Radom images*/
var description = [
  "assets/images/men-1.png",
  "assets/images/men-1.png",
  "assets/images/men-2.png",
  "assets/images/men-3.png"
];

var size = description.length
var x = Math.floor(size*Math.random())
document.getElementById('photos-radom').src=description[x];

/*Back to top*/
var btn = $('#button-top');

$(window).scroll(function() {
  if ($(window).scrollTop() > 800) {
    btn.addClass('show');
  } else {
    btn.removeClass('show');
  }
});

btn.on('click', function(e) {
  e.preventDefault();
  $('html, body').animate({scrollTop:0}, '300');
});

/*Nice scroll*/
$("body").niceScroll({
  horizrailenabled:false,
  scrollspeed:"90",
  cursorborder:"0",
  zindex: "99"
});

var $doc = $('html, body');
$('a').click(function() {
    $doc.animate({
        scrollTop: $( $.attr(this, 'href') ).offset().top
    }, 500);
    return false;
});

/*Slide owl carousel*/
$('.about-slide').owlCarousel({
  loop:true,
  margin:20,
  nav:true,
  navText: ["<svg version='1.1' id='Capa_1' width='18px' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px'viewBox='0 0 477.175 477.175' style='enable-background:new 0 0 477.175 477.175;' xml:space='preserve'><g><path d='M360.731,229.075l-225.1-225.1c-5.3-5.3-13.8-5.3-19.1,0s-5.3,13.8,0,19.1l215.5,215.5l-215.5,215.5c-5.3,5.3-5.3,13.8,0,19.1c2.6,2.6,6.1,4,9.5,4c3.4,0,6.9-1.3,9.5-4l225.1-225.1C365.931,242.875,365.931,234.275,360.731,229.075z'/></g></svg>","<svg version='1.1' id='Capa_1' width='18px' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px'viewBox='0 0 477.175 477.175' style='enable-background:new 0 0 477.175 477.175;' xml:space='preserve'><g><path d='M145.188,238.575l215.5-215.5c5.3-5.3,5.3-13.8,0-19.1s-13.8-5.3-19.1,0l-225.1,225.1c-5.3,5.3-5.3,13.8,0,19.1l225.1,225c2.6,2.6,6.1,4,9.5,4s6.9-1.3,9.5-4c5.3-5.3,5.3-13.8,0-19.1L145.188,238.575z'/></g></svg>"],
  autoplay:true,
  autoplayTimeout:5000,
  autoplayHoverPause:true,
  responsive:{
      0:{
          items:1
      },
      600:{
          items:2
      },
      1000:{
          items:2
      }
  }
});

$('.faq-slide').owlCarousel({
  loop:true,
  margin:20,
  nav:true,
  navText: ["<svg version='1.1' id='Capa_1' width='18px' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px'viewBox='0 0 477.175 477.175' style='enable-background:new 0 0 477.175 477.175;' xml:space='preserve'><g><path d='M360.731,229.075l-225.1-225.1c-5.3-5.3-13.8-5.3-19.1,0s-5.3,13.8,0,19.1l215.5,215.5l-215.5,215.5c-5.3,5.3-5.3,13.8,0,19.1c2.6,2.6,6.1,4,9.5,4c3.4,0,6.9-1.3,9.5-4l225.1-225.1C365.931,242.875,365.931,234.275,360.731,229.075z'/></g></svg>","<svg version='1.1' id='Capa_1' width='18px' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px'viewBox='0 0 477.175 477.175' style='enable-background:new 0 0 477.175 477.175;' xml:space='preserve'><g><path d='M145.188,238.575l215.5-215.5c5.3-5.3,5.3-13.8,0-19.1s-13.8-5.3-19.1,0l-225.1,225.1c-5.3,5.3-5.3,13.8,0,19.1l225.1,225c2.6,2.6,6.1,4,9.5,4s6.9-1.3,9.5-4c5.3-5.3,5.3-13.8,0-19.1L145.188,238.575z'/></g></svg>"],
  autoplay:false,
  autoplayTimeout:5000,
  autoplayHoverPause:true,
  responsive:{
      0:{
          items:1
      },
      600:{
          items:3
      },
      1000:{
          items:4
      }
  }
});





